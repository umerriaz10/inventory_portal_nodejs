import { Component } from '@angular/core';
import { JwtService } from 'src/app/services/jwt/jwt.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {

  constructor(
    private jwtService: JwtService,
    private router: Router
  ) {
    this.jwtService.destroyToken();
    this.router.navigate(['']);
  }

}
