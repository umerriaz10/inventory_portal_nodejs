import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-responsive';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Order } from 'src/app/interfaces/order';
import { ModalServiceService } from 'src/app/services/modalService/modal-service.service';
import { Router } from '@angular/router';
import { ViewInvoicePopupComponent } from 'src/app/components/view-invoice-popup/view-invoice-popup.component';
import { OrderItmes } from 'src/app/interfaces/order-itmes';
import { VendorsService } from 'src/app/services/vendors/vendors.service';

@Component({
  selector: 'app-view-orders',
  templateUrl: './view-orders.component.html',
  styleUrls: ['./view-orders.component.scss']
})
export class ViewOrdersComponent implements OnInit {

  /**
   * This variable is used to set configuration of the modal.
   *
   * @memberof TimeLogPopUpComponent
   */
  config = {
    backdrop: false,
    ignoreBackdropClick: false,
    keyboard: false
  };

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  orders: Array<Order> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  orderItems: Array<OrderItmes> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  vendor: Order = {};

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  pendingOrders: Array<Order> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  deliveredOrders: Array<Order> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  paidOrders: Array<Order> = [];

  @ViewChild('pendingDataTable', { static: true }) pendingTable;

  @ViewChild('deliveredDataTable', { static: true }) deliveredTable;

  @ViewChild('paidDataTable', { static: true }) paidTable;

  pendingTableRow;

  deliveredTableRow;

  paidTableRow;

  pendingDataTable: any;

  deliveredDataTable: any;

  paidDataTable: any;

  dtOption = {
    'paging':   false,
    'ordering': true,
    'info':     false,
    'responsive': {
      'details': true,
      'type': 'inline'
    }
  };

  isButtonClicked = false;

  constructor(
    private ngxService: NgxUiLoaderService,
    private orderService: OrdersService,
    private toastr: ToastrService,
    private vendorService: VendorsService,
    public matDialog: MatDialog,
    private router: Router,
    private modalService: ModalServiceService
  ) { }

  /**
   * This method is triggered when the component is initialized and get all the records
   *
   * @memberof InventoryHistoryComponent
   */
  ngOnInit() {
    this.getAllOrders();
  }

  /**
   * Get all the inventory records from firestore
   *
   * @memberof InventoryHistoryComponent
   */
  getAllOrders() {
    this.ngxService.start();
    const orders = this.orderService.getOrdersFromLocal();
    this.orders = [];
    this.pendingDataTable = $(this.pendingTable.nativeElement);
    this.deliveredDataTable = $(this.deliveredTable.nativeElement);
    this.paidDataTable = $(this.paidTable.nativeElement);
    this.pendingDataTable.DataTable().destroy();
    this.deliveredDataTable.DataTable().destroy();
    this.paidDataTable.DataTable().destroy();
    if (orders === undefined || orders === null) {
    this.orderService.getOrders().subscribe(res => {
      if (res.data) {
        res.data.forEach(rec => {
          this.orders.push({
            'date': rec.date,
            'vendor_name': rec.vendor_name,
            'vendor_id': rec.vendor_id,
            'no_of_products': rec.no_of_products,
            'city': rec.city,
            'status': rec.status,
            'order_type': rec.order_type,
            'total_amount': rec.total_amount,
            'id': rec.id,
            'orderedBy': rec.orderedBy
          });
        });
        this.divideOrders();
      }
    }, err => {
      this.toastr.error(err);
      this.ngxService.stop();
    });
  } else {
    this.orders = orders;
    this.divideOrders();
  }
  }

  refreshRecords () {
    this.orderService.saveOrdersLocalStorage(null);
    this.getAllOrders();
  }


  divideOrders() {
    this.pendingOrders = _.filter(this.orders, obj => obj.status === 'Pending');
    this.deliveredOrders = _.filter(this.orders, obj => obj.status === 'Delivered');
    this.paidOrders = _.filter(this.orders, obj => obj.status === 'Paid');
    this.setPendingOrdersDataTables();
    this.setDeliveredOrdersDataTables();
    this.setPaidOrdersDataTables();
    this.ngxService.stop();
  }

  setPendingOrdersDataTables() {
    this.pendingTableRow = this.pendingDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.pendingOrders,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'city' },
        { 'data': 'status' },
        { 'data': 'order_type'},
        { 'data': 'orderedBy'},
        { 'data': null,
          'defaultContent': '<button class="btn btn-secondary mr-1 expense-button">Invoice</button>'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewOrder(this.pendingDataTable, this.pendingTableRow);
  }

  setDeliveredOrdersDataTables() {
    this.deliveredTableRow = this.deliveredDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.deliveredOrders,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'city' },
        { 'data': 'status' },
        { 'data': 'order_type'},
        { 'data': 'orderedBy'},
        { 'data': null,
          'defaultContent': '<button class="btn btn-secondary mr-1 expense-button">Invoice</button>'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewOrder(this.deliveredDataTable, this.deliveredTableRow);
  }

  setPaidOrdersDataTables() {
    this.paidTableRow = this.paidDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.paidOrders,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'city' },
        { 'data': 'status' },
        { 'data': 'order_type'},
        { 'data': 'orderedBy'},
        { 'data': null,
          'defaultContent': '<button class="btn btn-secondary mr-1 expense-button">Invoice</button>'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewOrder(this.paidDataTable, this.paidTableRow);
  }

   viewOrder(dataTable, tableRow) {
     const self = this;
    $(dataTable).on( 'click', 'button', function () {
      self.isButtonClicked = true;
      const data = tableRow.row( $(this).parents('tr') ).data();
      self.getAllinvoicesObject(data);
    });
    $(dataTable).on( 'click', 'tr', function (e) {
      const data = tableRow.row(this).data();
      if (self.isButtonClicked === true) {
        e.stopPropagation();
        self.isButtonClicked = false;
      } else {
        self.router.navigate(['main/view-orders/order-detail/' + data.vendor_name + '/' + data.id]);
      }
  });
  }

  getAllinvoicesObject(data) {
    this.vendorService.getVendorById(data.vendor_id).subscribe(res => {
      this.vendor = res.data[0];
      this.orderService.getOrderItemById(data.id).subscribe(response => {
        this.orderItems = response.data;
        this.openInvoicePopup(data);
      }, err => {
        this.toastr.error(err);
      });
    }, err => {
      this.toastr.error(err);
    });
  }

  openInvoicePopup(data) {
    const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = false;
  dialogConfig.id = 'modal-component';
  dialogConfig.height = '80vh';
  dialogConfig.width = 'auto';
  dialogConfig.data = {'order': data, 'orderItems': this.orderItems, 'vendor': this.vendor};
  const modalDialog = this.matDialog.open(ViewInvoicePopupComponent, dialogConfig);
  }

}
