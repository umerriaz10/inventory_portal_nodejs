import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { VendorsService } from 'src/app/services/vendors/vendors.service';
import { Vendors } from 'src/app/interfaces/vendors';
import { IOption } from 'ng-select';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { Payment } from 'src/app/interfaces/payment';
import { UserService } from 'src/app/services/user/user.service';
import * as _ from 'lodash';
import { PaymentsService } from 'src/app/services/payments/payments.service';
import { NgSelectComponent } from '@ng-select/ng-select';

@Component({
  selector: 'app-new-payment',
  templateUrl: './new-payment.component.html',
  styleUrls: ['./new-payment.component.scss']
})
export class NewPaymentComponent implements OnInit {

  paymentForm;

  payment: Payment = {};

  payType = '';

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  vendorsDropdown: Array<IOption> = [];

  vendors: Array<Vendors> = [];

  vendor: Vendors = {};

  ordersDropdown: Array<IOption> = [];

  orders: Array<Order> = [];

  order: Order = {};

  paymentTypes: string[] = ['Receiving', 'Paying'];

  isPaymentByOrder = false;

  isVendorSelected = false;

  @ViewChild('myDateInput', {static: false}) ngSelectComponent: NgSelectComponent;

  /**
   * Holds the date that user selects
   *
   * @memberof OrderDetailComponent
   */
  selectedDate = new Date();

  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private vendorsService: VendorsService,
    private ordersService: OrdersService,
    private userService: UserService,
    private paymentServices: PaymentsService,
    private toastr: ToastrService,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    this.createForm();
    this.getvendors();
  }

  getvendors() {
    const vendors = [];
    this.vendorsService.getVendors().subscribe(res => {
      this.vendors = res.data;
      this.vendors.forEach(vendor => {
        vendors.push({
          'value': vendor.id,
          'label': vendor.v_name
        });
      });
      this.vendorsDropdown = vendors;
    });
  }

  vendorIsSelected(event) {
    if (event !== undefined) {
      this.isVendorSelected = true;
    this.vendor = _.filter(this.vendors, obj => obj.id === event.value)[0];
    this.payment.vendor_id = this.vendor.id;
    this.payment.vendor_name = this.vendor.v_name;
    if (this.isPaymentByOrder === true) {
      this.getOrdersByVendorId(this.vendor.id);
    }
    }
  }

  getOrdersByVendorId(vendorId) {
    const orders = [];
    this.ordersService.getOrdersByVendorId(vendorId).subscribe(res => {
      this.orders = res.data;
      this.orders.forEach(order => {
        orders.push({
          'value': order.id,
          'label': order.date + '-' + order.city + '-' + order.status + '-' + order.order_type + '-' + order.total_amount
        });
      });
      this.ordersDropdown = orders;
    });
  }

  orderIsSelected(event) {
    if (event !== undefined) {
      this.order = _.filter(this.orders, obj => obj.id === event.value)[0];
      this.order.status = 'Paid';
      this.paymentForm.get('amount').setValue(this.order.total_amount);
      this.payment.order_id = this.order.id;
    }
  }

  checkPaymentForm() {
    this.ngxService.start();
    this.submitted = true;
    if (this.payType === '') {
      this.toastr.error('Please Select the Type of Payment');
      this.ngxService.stop();
    } else if (this.isVendorSelected === false) {
      this.toastr.error('Please Select any Vendor');
      this.ngxService.stop();
    } else if (this.paymentForm.invalid) {
      this.toastr.error('Please enter the required Amount');
      this.ngxService.stop();
    } else {
      this.setValues(this.paymentForm.controls);
    }
  }

  addPayment() {
    this.paymentServices.postPayment(this.payment).subscribe(res => {
      this.updateVendor();
    }, err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  updateVendor() {
    this.vendorsService.updateVendor(this.vendor).subscribe(res => {
      if (this.payment.order_id !== null) {
        this.updateOrderStatus();
      }
      this.resetForm();
      this.toastr.success('Payment ' + this.payType + ' Successfully');
      this.ngxService.stop();
    }, err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  resetForm() {
    this.payType = '';
    this.paymentForm.get('amount').reset();
    this.submitted = false;
  }

  updateOrderStatus() {
    this.ordersService.updateOrder(this.order).subscribe(res => {
      this.resetForm();
      this.toastr.success('Payment ' + this.payType + ' Successfully');
      this.ngxService.stop();
    }, err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  setValues(form) {
    if (this.payType === 'Receiving') {
      this.payment.receive = true;
      this.vendor.v_balance = this.vendor.v_balance - form.amount.value;
      this.vendor.v_totalPaid = this.vendor.v_totalPaid + form.amount.value;
      this.payment.pay = false;
    } else {
      this.vendor.v_paidToVendor = this.vendor.v_paidToVendor + form.amount.value;
      this.payment.pay = true;
      this.payment.receive = false;
    }
    if (!this.payment.order_id) {
      this.payment.order_id = null;
    }
    this.payment.date = this.selectedDate;
    this.payment.amount = form.amount.value;
    this.payment.addedBy = this.userService.getUser().name;
    this.addPayment();
  }

  /**
   * This method creates the form with all the relevant controls
   *
   * @memberof TimeLogDetailComponent
   */
  createForm() {
    this.paymentForm = this.formBuilder.group({
      amount: ['', [Validators.required, Validators.min(0)]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.paymentForm.controls; }

  /**
   * This method is triggered when user clicks on any of the dropdowns in 'country', 'state', and 'city'
   * in order to keep the dropdown from opening upwards
   *
   * @param value
   * @memberof RegisterComponent
   */
  scrollOnDropdownExpansion(value) {
    window.scrollTo(0, value);
  }

  slided() {
    this.isPaymentByOrder = !this.isPaymentByOrder;
    this.paymentForm.reset();
  }

}
