import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { JwtService } from 'src/app/services/jwt/jwt.service';

/**
 * This component is rendered at the start of application, it provides the UI
 * & functionality for the login page.
 *
 * This class is used to build a login form along with initialization of validators
 * as well as authenticate the user, and reroute upon success
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

 /**
  * This property initializes the formGroup element.
  */
 userForm: FormGroup;

 /**
  * This property when true displays the spinner on submit button.
  *
  * @type {Boolean}
  * @memberof RegisterComponent
  */
 isLoading = false;

 /**
  * Creates an instance of LoginComponent.
  * @param {FormBuilder} fb
  * @param {FireAuthService} fauth
  * @param {UserService} userService
  * @param {Router} router
  * @param {ToastrService} toastr
  * @memberof LoginComponent
  */
 constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private jwt: JwtService,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService) {
      this.checkForSession();
    }

  /**
   * Called on the initializing of the component
   *
   * @memberof LoginComponent
   */
  ngOnInit() {
    this.buildForm();
  }

  /**
   * Checks for an existing session. If the session exists
   *
   */
  checkForSession() {
    if (localStorage.getItem('token')) {
      this.router.navigate(['main']);
    }
  }

  /**
   * This method returns the values of the form controls.
   *
   * @return
   */
  get form() { return this.userForm.controls; }

  /**
   * This method initialized the the formGroup element. Its properties and the validators.
   *
   * @method buildForm
   * @return
   */
  buildForm() {
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  /**
   * Method to get the user login and get the required user data
   *
   * @memberof LoginComponent
   */
  login() {
    this.isLoading = true;
    const cred = {'email': this.form.username.value, 'password' : this.form.password.value};
    this.userService.login(cred).subscribe(res => {
      debugger
      this.userService.saveUser(res.data);
      this.jwt.saveToken(res.token);
      this.router.navigate(['main']);
      this.toastr.success('Successfully logged In');
      this.isLoading = false;
    }, err => {
      this.toastr.error(err);
      this.isLoading = false;
    });
  }
}
