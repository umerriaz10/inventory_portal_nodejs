import { Component, OnInit, ViewChild } from '@angular/core';
import { Items } from 'src/app/interfaces/items';
import { ItemsService } from 'src/app/services/items/items.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import 'datatables.net';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddProductPopupComponent } from 'src/app/components/add-product-popup/add-product-popup.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  /**
   * Holds the items coming from DB
   *
   * @type {Array<Items>}
   * @memberof ProductsComponent
   */
  items: Array<Items> = [];

  /**
   * Holds the items for data table
   *
   * @type {Array<Items>}
   * @memberof ProductsComponent
   */
  itemsDatatable: Array<Items> = [];

  @ViewChild('dataTable', { static: true }) table;

  /**
   * Holds the datatable object
   *
   * @type {*}
   * @memberof ProductsComponent
   */
  dataTable: any;

  /**
   * Holds the datatable options
   *
   * @memberof ProductsComponent
   */
  dtOption = {
    'paging': false,
    'ordering': true,
    'info': false
  };

  tableRow;

  /**
   * Creates an instance of ProductsComponent.
   *
   * @param {ItemsService} itemService
   * @param {NgxUiLoaderService} ngxService
   * @memberof ProductsComponent
   */
  constructor(
    private itemService: ItemsService,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private router: Router,
    public matDialog: MatDialog
  ) { }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof ProductsComponent
   */
  ngOnInit() {
    this.ngxService.start();
    this.dataTable = $(this.table.nativeElement);
    this.getProducts(this.setDataTable.bind(this), false);
  }

  getProducts(callBack, isDestroy) {
    this.itemsDatatable = [];
    const products = this.itemService.getItemsFromLocal();
    if (products === undefined || products === null) {
      this.itemService.getItems().subscribe(res => {
        this.itemService.saveItemsLocalStorage(res.data);
        this.items = res.data;
        if (isDestroy === false) {
          callBack();
        } else {
          this.dataTable.DataTable().destroy();
          callBack();
        }
      }, err => {
        this.toastr.error(err);
      });
    } else {
      this.items = products;
      if (isDestroy === false) {
        callBack();
      } else {
        this.dataTable.destroy();
        callBack();
      }
    }
  }

  /**
   * Sets the data table for products
   *
   * @param {*} items
   * @memberof ProductsComponent
   */
  setDataTable() {
    this.items.forEach(element => {
      this.itemsDatatable.push({
        'p_name': element.p_name,
        'p_quantity': element.p_quantity,
        'p_price': element.p_price,
        'p_addedBy': element.p_addedBy,
        'id': element.id
      });
    });
    this.tableRow = this.dataTable.DataTable({
      dtOption: this.dtOption,
      data: this.itemsDatatable,
      columns: [
        { 'data': 'p_name' },
        { 'data': 'p_quantity' },
        { 'data': 'p_price'},
        { 'data': 'p_addedBy' }
        // { 'data': null,
        //   'defaultContent': '<button class="btn btn-secondary mr-1 expense-button">Detail</button>'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewProduct();
    this.ngxService.stop();
  }

  viewProduct() {
    const self = this;
   $(self.dataTable).on( 'click', 'tr', function () {
     const data = self.tableRow.row(this).data();
     self.router.navigate(['main/products/product-detail/' + data.p_name + '/' + data.id]);
 });
 }

  /**
   * Method that opens the Add a product popup
   *
   * @memberof OrderDetailComponent
   */
  openAddVendorPopup() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = 'auto';
    dialogConfig.data = {'function': this.getProducts.bind(this, this.setDataTable.bind(this), true)};
    const modalDialog = this.matDialog.open(AddProductPopupComponent, dialogConfig);
  }

}
