import { Component, OnInit, ViewChild } from '@angular/core';
import { InventoryRecord } from 'src/app/interfaces/inventory-record';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { InventoryRecordService } from 'src/app/services/inventory-record/inventory-record.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-responsive';
import { ViewInventoryRecPopupComponent } from 'src/app/components/view-inventory-rec-popup/view-inventory-rec-popup.component';

/**
 * This component is responsible for manipulating the inventory history data.
 */
@Component({
  selector: 'app-inventory-history',
  templateUrl: './inventory-history.component.html',
  styleUrls: ['./inventory-history.component.scss']
})
export class InventoryHistoryComponent implements OnInit {

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<InventoryRecord>}
   * @memberof InventoryHistoryComponent
   */
  inventoryRecords: Array<InventoryRecord> = [];

  /**
   * This array is used to set the icon and sorting direction of data table
   *
   * @memberof TimeLogMainComponent
   */
  sortingArray = [
    {
      key: 'date',
      selected: false,
      asc: false,
      desc: false
    },
    {
      key: 'item',
      selected: false,
      asc: false,
      desc: false
    },
    {
      key: 'quantity',
      selected: false,
      asc: false,
      desc: false
    }
  ];

  @ViewChild('dataTable', { static: true }) table;

  dataTable: any;

  dtOption = {
    'paging':   false,
    'ordering': true,
    'info':     false,
    'responsive': {
      'details': true,
      'type': 'inline'
    }
  };

  tableRow;

  invRecItems;

  constructor(
    private ngxService: NgxUiLoaderService,
    private inventoryRecordService: InventoryRecordService,
    private toastr: ToastrService,
    public matDialog: MatDialog,
    private invRecService: InventoryRecordService
  ) { }

  /**
   * This method is triggered when the component is initialized and get all the records
   *
   * @memberof InventoryHistoryComponent
   */
  ngOnInit() {
    this.getAllInventoryRecords();
  }

  /**
   * Get all the inventory records from firestore
   *
   * @memberof InventoryHistoryComponent
   */
  getAllInventoryRecords() {
    this.ngxService.start();
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable().destroy();
    this.inventoryRecords = [];
    const invRec = this.invRecService.getInvRecordsFromLocal();
    if (invRec === undefined || invRec === null) {
    this.inventoryRecordService.getInventoryRecords().subscribe(res => {
      if (res.data) {
        res.data.forEach(rec => {
          this.inventoryRecords.push({
            'id': rec.id,
            'date': rec.date,
            'no_of_products': rec.no_of_products,
            'status': rec.status,
            'addedBy': rec.addedBy
          });
        });
        this.setDatable();
      }
    }, err => {
      this.toastr.error(err);
      this.ngxService.stop();
    });
  } else {
    this.inventoryRecords = invRec;
    this.setDatable();
  }
  }

  refreshRecords () {
    this.invRecService.saveInvRecordsLocalStorage(null);
    this.getAllInventoryRecords();
  }


  setDatable() {
    this.tableRow = this.dataTable.DataTable({
      dtOption: this.dtOption,
      data: this.inventoryRecords,
      columns: [
        { 'data': 'date' },
        { 'data': 'no_of_products' },
        { 'data': 'status' },
        { 'data': 'addedBy' },
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details of the desired record',
      }});
      this.viewInvRecord();
      this.ngxService.stop();
  }

  viewInvRecord() {
    const self = this;
   $(self.dataTable).on( 'click', 'tr', function () {
     const data = self.tableRow.row(this).data();
     self.getOrderItems(data);
 });
 }

 openInventoryRecPopup(data) {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = false;
  dialogConfig.id = 'modal-component';
  dialogConfig.height = 'auto';
  dialogConfig.width = 'auto';
  dialogConfig.data = {'data': data, 'products': this.invRecItems};
  const modalDialog = this.matDialog.open(ViewInventoryRecPopupComponent, dialogConfig);
 }

 getOrderItems(data) {
  this.invRecService.getInventoryRecordItemsById(data.id).subscribe(res => {
    this.invRecItems = res.data;
    this.openInventoryRecPopup(data);
  }, err => {
    this.toastr.error(err);
    this.ngxService.stop();
  });
 }

  /**
   * This method is used to set icon values based on the selected column and sorting order. It also resets the other column icons.
   *
   * @param {string} key
   * @param {function} method
   * @memberof TimeLogMainComponent
   */
  selectedColumn(key, method) {
    // tslint:disable-next-line: one-variable-per-declaration
    const object = _.filter(this.sortingArray, (obj: { key: any; }) => obj.key === key),
    diff = _.differenceWith(this.sortingArray, object);
    diff.forEach(obj => { obj.selected = obj.asc = obj.desc = false; });
    object[0].selected = true;
    if (object[0].selected) {
      if (object[0].asc) {
        object[0].asc = false;
        object[0].desc = true;
        method('desc');
      } else {
        object[0].desc = false;
        object[0].asc = true;
        method('asc');
      }
    }
  }

  /**
   * This method is used to sort the data table according to date in ascending or descending order, according to the provided parameter
   *
   * @param {string} sortDirection
   * @memberof TimeLogMainComponent
   */
  sortByDate(sortDirection) {
    this.inventoryRecords = _.orderBy(this.inventoryRecords, ['date'], [sortDirection]);
  }

  /**
   * This method is used to sort the data table according to projects in ascending or descending order, according to the provided parameter
   *
   * @param {string} sortDirection
   * @memberof TimeLogMainComponent
   */
  sortByItem(sortDirection) {
      this.inventoryRecords = _.orderBy(this.inventoryRecords, ['itemName'], [sortDirection]);
  }

  /**
   * This method is used to sort the data table according to positions in ascending or descending order, according to the provided parameter
   *
   * @param {string} sortDirection
   * @memberof TimeLogMainComponent
   */
  sortByQuantity(sortDirection) {
      this.inventoryRecords = _.orderBy(this.inventoryRecords, ['quantity'], [sortDirection]);
  }
}
