import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-header-brand',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  /**
   * User's name field in header
   */
  public username: string;

  /**
   * Class constructor is used to inject user service in the component.
   *
   * @param userService
   */
  constructor(private userService: UserService) {}

  /**
   * This method is used for initializing component's attributes
   */
  ngOnInit() {
    this.setUsername();
  }

  /**
   * This method is used to set username field to logged in user's full name.
   */
  setUsername() {
    this.username = this.userService.getUser().name;
  }
}
