import { Component, OnInit, ViewChild } from '@angular/core';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { PaymentsService } from 'src/app/services/payments/payments.service';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as moment from 'moment';

@Component({
  selector: 'app-company-summary',
  templateUrl: './company-summary.component.html',
  styleUrls: ['./company-summary.component.scss']
})
export class CompanySummaryComponent implements OnInit {

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('startingDateInput', {static: false}) startingDateInput: jqxDateTimeInputComponent;

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('endingDateInput', {static: false}) endingDateInput: jqxDateTimeInputComponent;

  startingDate;

  endingDate;

  startingDateValidation = false;

  /**
   * This variable is used to set the max date in the datepicker
   *
   * @memberof TimeLogPopUpComponent
   */
  maxDate = new Date();

  totalIncome = 0;

  amountReceived = 0;

  amountPaid = 0;

  allOrders = [];

  allPayments;

  paidPayments;
  receivedPayments;

  constructor(
    private orderService: OrdersService,
    private paymentService: PaymentsService,
    private toastr: ToastrService,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
    this.getTotalAmountByOrders();
  }

  getTotalAmountByOrders() {
    this.ngxService.start();
    this.orderService.getOrders().subscribe(res => {
      this.allOrders = res.data;
      this.calTotalIncome();
    }, err => {
      this.ngxService.stop();
      this.toastr.error('Please Refresh due to this error:' + err);
    });
  }

  refreshedRecords() {
    this.startingDateInput.setDate('');
    this.endingDateInput.setDate('');
    this.getTotalAmountByOrders();
  }

  getAmountPaidAndReceivedByPayments() {
    this.paymentService.getPayments().subscribe(res => {
      this.allPayments = res.data;
      this.calAmountPaidAndReceived();
    }, err => {
      this.ngxService.stop();
      this.toastr.error('Please Refresh due to this error:' + err);
    });
  }

  calTotalIncome() {
    this.totalIncome = 0;
    this.allOrders.forEach(order => {
      this.totalIncome += order.total_amount;
    });
    this.getAmountPaidAndReceivedByPayments();
  }

  calAmountPaidAndReceived() {
    this.amountPaid = this.amountReceived = 0;
    this.receivedPayments = _.filter(this.allPayments, obj => obj.receive === 1);
    this.paidPayments = _.filter(this.allPayments, obj => obj.pay === 1);
    this.paidPayments.forEach(payment => {
      this.amountPaid += payment.amount;
    });
    this.receivedPayments.forEach(payment => {
      this.amountReceived += payment.amount;
    });
    this.ngxService.stop();
  }

  /**
   * This method is used for validating the starting date, set the error if invalid, and reset the ending date value
   *
   * @memberof RequestLeavePopupComponent
   */
  startingDateSelected(event) {
    this.endingDateInput.setDate('');
    if (event === null && this.startingDateInput === null) {
      this.toastr.error('Starting Date is Required');
      this.startingDateValidation = false;
    } else if (Number.isNaN(event.args.date)) {
      this.toastr.error('Please enter a valid date');
      this.startingDateValidation = false;
    } else {
      this.startingDateValidation = true;
    }
  }

   /**
   * This method is used for validating the ending date, set the error if invalid
   *
   * @memberof RequestLeavePopupComponent
   */
  endingDateSelected(event, value) {
    if (event === null && this.endingDateInput === null) {
      this.toastr.error('Ending Date is Required');
    } else if (Number.isNaN(event.args.date)) {
      this.toastr.error('Please enter a valid Ending Date');
    } else if (this.startingDateInput.val() !== null &&
    this.startingDateInput.getDate() > this.endingDateInput.getDate()) {
        this.toastr.error('Ending Date cannot be less than Starting Date');
        this.endingDateInput.setDate('');
    } else {
      this.endingDate = event.args.date.setHours(0, 0, 0, 0);
      this.filterOrder();
    }
  }

  filterOrder() {
    this.ngxService.start();
    this.amountPaid = this.amountReceived = this.totalIncome = 0;
    let date;
    this.startingDate = new Date(this.startingDate).setHours(0, 0, 0, 0);
    this.allOrders.forEach(order => {
      date = new Date(order.date).setHours(0, 0, 0, 0);
      if (date >= this.startingDate && date <= this.endingDate) {
        this.totalIncome += order.total_amount;
      }
    });
    this.filterPayments();
  }

  filterPayments() {
    let date;
    this.allPayments.forEach(payment => {
      date = new Date(payment.date).setHours(0, 0, 0, 0);
      if (date >= this.startingDate && date <= this.endingDateInput) {
        if (payment.pay === 1) {
          this.amountPaid += payment.amount;
        } else {
          this.amountReceived += payment.amount;
        }
      }
    });
    this.ngxService.stop();
  }

}
