import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router, ActivatedRoute } from '@angular/router';
import { VendorsService } from 'src/app/services/vendors/vendors.service';
import { Vendors } from 'src/app/interfaces/vendors';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Order } from 'src/app/interfaces/order';
import * as _ from 'lodash';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-responsive';
import { ToastrService } from 'ngx-toastr';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { ViewOrderDetailPopupComponent } from 'src/app/components/view-order-detail-popup/view-order-detail-popup.component';
import { Payment } from 'src/app/interfaces/payment';
import { PaymentsService } from 'src/app/services/payments/payments.service';

@Component({
  selector: 'app-vendor-detail',
  templateUrl: './vendor-detail.component.html',
  styleUrls: ['./vendor-detail.component.scss']
})
export class VendorDetailComponent implements OnInit {

  vendor: Vendors;

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof TimeLogDetailComponent
   */
  vendorForm: FormGroup;

  /**
   * Flag to switch between edit and view mode
   *
   * @memberof OrderDetailComponent
   */
  isEdit = false;

  submitted = false;

    /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  vendorOrders: Array<Order> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  pendingOrders: Array<Order> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  deliveredOrders: Array<Order> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  paidOrders: Array<Order> = [];

  @ViewChild('pendingDataTable', { static: true }) pendingTable;

  @ViewChild('deliveredDataTable', { static: true }) deliveredTable;

  @ViewChild('paidDataTable', { static: true }) paidTable;

  @ViewChild('paidPaymentDataTable', { static: true }) paidPaymentTable;

  @ViewChild('receivedPaymentDataTable', { static: true }) receivedPaymentTable;

  pendingTableRow;

  deliveredTableRow;

  paidTableRow;

  paidPaymentTableRow;

  receivedPaymentTableRow;

  pendingDataTable: any;

  deliveredDataTable: any;

  paidDataTable: any;

  paidPaymentDataTable: any;

  receivedPaymentDataTable: any;

  dtOption = {
    'paging':   false,
    'ordering': true,
    'info':     false,
    'responsive': {
      'details': true,
      'type': 'inline'
    }
  };

  orderItems;

  payments: Array<Payment> = [];

  receivedPayments: Array<Payment> = [];

  paidPayments: Array<Payment> = [];

  isValid = false;


  constructor(
    private ngxService: NgxUiLoaderService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private vendorService: VendorsService,
    private paymentService: PaymentsService,
    public matDialog: MatDialog,
    private orderService: OrdersService,
    private router: Router,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getVendorById(params.id);
      this.getOrdesByVendorId(params.id);
      this.getVendorPayments(params.id);
    });
  }

  createForm() {
    this.vendorForm = this.formBuilder.group({
      v_name: [this.vendor.v_name, Validators.required],
      v_email: [this.vendor.v_email],
      v_address: [this.vendor.v_address],
      v_contact_number: [this.vendor.v_contact_number]
    });
  }

  get f() { return this.vendorForm.controls; }

  /**
   * This method is used to get the main order on the basis of id
   *
   * @param {*} id
   * @memberof OrderDetailComponent
   */
  getVendorById(id) {
    this.ngxService.start();
    this.vendorService.getVendorById(id).subscribe(response => {
      this.vendor = response.data[0];
      this.createForm();
      this.ngxService.stop();
    }, err => {
      this.errorGettingOrders(err);
    });
  }

  getVendorPayments(id) {
    this.paymentService.getPaymentsById(id).subscribe(res => {
      this.payments = res.data;
      this.dividePayments();
    }, err => {
      this.toastr.error(err);
    });
  }

  getOrdesByVendorId(id) {
    this.pendingDataTable = $(this.pendingTable.nativeElement);
    this.deliveredDataTable = $(this.deliveredTable.nativeElement);
    this.paidDataTable = $(this.paidTable.nativeElement);
    this.ngxService.start();
    this.orderService.getOrdersByVendorId(id).subscribe(res => {
      this.vendorOrders = res.data;
      this.divideOrders();
    }, err => {
      this.errorGettingOrders(err);
    });
  }

  /**
   * Method that checks the emptiness of order items in an order
   *
   * @memberof OrderDetailComponent
   */
  detectFormChanges() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.vendorForm.dirty === false) {
      this.isValid = false;
      this.toastr.warning('Please apply some changes!');
      this.ngxService.stop();
    } else if (this.vendorForm.invalid) {
      this.toastr.error('Please Check Your Form Fields!');
      this.isValid = false;
      this.ngxService.stop();
    } else if (this.isValid === true) {
      this.setVendor();
    }
  }

  updateVendor() {
    this.vendorService.updateVendor(this.vendor).subscribe(res => {
      this.successMessageDisplay('Vendor is successfully updated!');
    }, err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  setVendor() {
    this.vendor.v_name = this.vendorForm.get('v_name').value;
    this.vendor.v_address = this.vendorForm.get('v_address').value;
    this.vendor.v_contact_number = this.vendorForm.get('v_contact_number').value;
    this.vendor.v_email = this.vendorForm.get('v_email').value;
    this.updateVendor();
  }

  /**
   * This method is used to display message for successful entry and
   * reroutes the user to orders list view
   *
   * @memberof OrderDetailComponent
   */
  successMessageDisplay(msg) {
    this.toastr.success(msg);
    this.ngxService.stop();
    this.router.navigate(['/main/view-vendors/']);
  }

  divideOrders() {
    this.pendingOrders = _.filter(this.vendorOrders, obj => obj.status === 'Pending');
    this.deliveredOrders = _.filter(this.vendorOrders, obj => obj.status === 'Delivered');
    this.paidOrders = _.filter(this.vendorOrders, obj => obj.status === 'Paid');

    this.setPendingOrdersDataTables();
    this.setDeliveredOrdersDataTables();
    this.setPaidOrdersDataTables();
    this.ngxService.stop();
  }

    /**
   * Method to be called when API causes error in getting orders
   *
   * @param {*} err
   * @memberof OrderDetailComponent
   */
  errorGettingOrders(err) {
    this.toastr.error(err);
    this.router.navigate(['/main/view-vendors/']);
    this.ngxService.stop();
  }

  dividePayments() {
    this.paidPaymentDataTable = $(this.paidPaymentTable.nativeElement);
    this.receivedPaymentDataTable = $(this.receivedPaymentTable.nativeElement);
    this.paidPayments = _.filter(this.payments, obj => obj.pay === 1);
    this.deliveredOrders = _.filter(this.payments, obj => obj.receive === 1);

    this.setPaidPayments();
    this.setReceivedPayments();
    this.ngxService.stop();
  }

  setPaidPayments() {
    this.paidPaymentTableRow = this.paidPaymentDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.paidPayments,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'amount' },
        { 'data': 'addedBy' }
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details',
      }});
  }

  setReceivedPayments() {
    this.receivedPaymentTableRow = this.receivedPaymentDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.receivedPayments,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'amount' },
        { 'data': 'addedBy' }
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details',
      }});
  }

  setPendingOrdersDataTables() {
    this.pendingTableRow = this.pendingDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.pendingOrders,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'city' },
        { 'data': 'status' },
        { 'data': 'order_type' },
        { 'data': 'total_amount'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewOrder(this.pendingDataTable, this.pendingTableRow);
  }

  setDeliveredOrdersDataTables() {
    this.deliveredTableRow = this.deliveredDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.deliveredOrders,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'city' },
        { 'data': 'status' },
        { 'data': 'order_type' },
        { 'data': 'total_amount'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewOrder(this.deliveredDataTable, this.deliveredTableRow);
  }

  setPaidOrdersDataTables() {
    this.paidTableRow = this.paidDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.paidOrders,
      columns: [
        { 'data': 'date' },
        { 'data': 'vendor_name' },
        { 'data': 'city' },
        { 'data': 'status' },
        { 'data': 'order_type' },
        { 'data': 'total_amount'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewOrder(this.paidDataTable, this.paidTableRow);
  }

   viewOrder(dataTable, tableRow) {
     const self = this;
     $(dataTable).on( 'click', 'tr', function () {
      const data = tableRow.row(this).data();
      self.getOrderItems(data);
    });
  }

  getOrderItems(data) {
    this.orderService.getOrderItemById(data.id).subscribe(res => {
      this.orderItems = res.data;
      this.openOrderDetailPopup(data);
    }, err => {
      this.toastr.error(err);
      this.ngxService.stop();
    });
   }

   openOrderDetailPopup(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = 'auto';
    dialogConfig.data = {'data': data, 'products': this.orderItems};
    const modalDialog = this.matDialog.open(ViewOrderDetailPopupComponent, dialogConfig);
   }

   /**
   * This method allows to toggle between edit mode, disable some fields
   *
   * @memberof OrderDetailComponent
   */
  onEditBtn() {
    this.isEdit = !this.isEdit;
  }

}
