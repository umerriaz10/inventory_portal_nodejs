import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IOption } from 'ng-select';
import { ProductPopupComponent } from 'src/app/components/product-popup/product-popup.component';
import { OrderItmes } from 'src/app/interfaces/order-itmes';
import * as _ from 'lodash';
import { ItemsService } from 'src/app/services/items/items.service';
import { ConfirmationPopupComponent } from 'src/app/components/confirmation-popup/confirmation-popup.component';
import { VendorsService } from 'src/app/services/vendors/vendors.service';
import { Vendors } from 'src/app/interfaces/vendors';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  /**
   * Variable that holds the order object
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  order: any;

  /**
   * Array of orderItems
   *
   * @type {Array<OrderItmes>}
   * @memberof OrderDetailComponent
   */
  orderItems: Array<OrderItmes> = [];

  /**
   * This variable is used to store all order items to be removed
   *
   * @type {Array<TimeLog>}
   * @memberof OrderDetailComponent
   */
  orderItemsToBeRemoved: Array<any> = [];

  /**
   * This variable is used to store all order items to be added
   *
   * @type {Array<TimeLog>}
   * @memberof OrderDetailComponent
   */
  orderItemsToBeAdded: Array<any> = [];

  /**
   * Flag to switch between edit and view mode
   *
   * @memberof OrderDetailComponent
   */
  isEdit = false;

  /**
   * Holds the products data
   *
   * @memberof OrderDetailComponent
   */
  items;

  /**
   * holds the bulk order items
   *
   * @type {InventoryRecord}
   * @memberof OrderDetailComponent
   */
  bulkOrderItems: Array<any> = [];

  /**
   * This array holds the options for products dropdown.
   *
   * @type {Array < IOption >}
   * @memberof OrderDetailComponent
   */
  products: Array<IOption> = [];

  /**
   * Object for defining confirmation popup attributes
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  confirmationPopupObject: any = {
    'msg': '',
    'cancel': '',
    'confirm': ''
  };

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  cities: Array<IOption> = [{
    'value': 'LHR',
    'label': 'LHR'
  }, {
    'value': 'RWP',
    'label': 'RWP'
  }, {
    'value': 'PEW',
    'label': 'PEW'
  }];

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  statuses: Array<IOption> = [{
    'value': 'Pending',
    'label': 'Pending'
  }, {
    'value': 'Delivered',
    'label': 'Delivered'
  }, {
    'value': 'Paid',
    'label': 'Paid'
  }];

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  orderTypes: Array<IOption> = [{
    'value': 'Cash',
    'label': 'Cash'
  }, {
    'value': 'Credit',
    'label': 'Credit'
  }];

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof OrderDetailComponent
   */
  orderForm: FormGroup;

  /**
   * Flag that indicates whether the order is changed or not
   *
   * @memberof OrderDetailComponent
   */
  isChangeFlag = false;

 /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  vendors: Array<IOption> = [];

  /**
   * Holds the selected vendor information
   *
   * @type {Vendors}
   * @memberof OrderDetailComponent
   */
  vendor: Vendors = {};

  /**
   * Holds the value of previous total amount of the order
   *
   * @memberof OrderDetailComponent
   */
  preAmount = 0;


  constructor(
    private route: ActivatedRoute,
    private orderService: OrdersService,
    private router: Router,
    private toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    private itemService: ItemsService,
    private vendorsService: VendorsService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    public matDialog: MatDialog
  ) {
  }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof OrderDetailComponent
   */
  ngOnInit() {
    this.ngxService.start();
    this.route.params.subscribe(params => {
      this.getOrderById(params.id);
    });
    this.getProducts();
  }

  /**
   * This method creates the form with all the relevant controls and validations.
   *
   * @memberof RegisterComponent
   */
  createForm() {
    this.orderForm = this.formBuilder.group({
      vendor: [null, [Validators.required]],
      city: [null, [Validators.required]],
      status: [null, [Validators.required]],
      orderType: [null, [Validators.required]]
    });
  }

  get f() { return this.orderForm.controls; }

  /**
   * Get the products for dropdown
   *
   * @memberof OrderDetailComponent
   */
  getProducts() {
    this.itemService.getItems().subscribe(res => {
      this.items = res.data;
      this.setProducts();
    });
  }

  /**
   * Updates the flag if any dropdown values changes
   *
   * @memberof OrderDetailComponent
   */
  dropDownValueChanged() {
    this.isChangeFlag = true;
  }

  /**
   * Sets the dropdown values for products
   *
   * @memberof OrderDetailComponent
   */
  setProducts() {
    const products = [];
    this.items.forEach(item => {
      products.push({
        'label': item.p_name,
        'value': item.id
      });
    });
    this.products = products;
  }

  /**
   * This method is triggered when user clicks on any of the dropdowns in 'country', 'state', and 'city'
   * in order to keep the dropdown from opening upwards
   *
   * @param value
   * @memberof OrderDetailComponent
   */
  scrollOnDropdownExpansion(value) {
    window.scrollTo(0, value);
  }

  /**
   * This method is used to get the main order on the basis of id
   *
   * @param {*} id
   * @memberof OrderDetailComponent
   */
  getOrderById(id) {
    this.orderService.getOrderById(id).subscribe(response => {
      this.order = response.data[0];
      this.preAmount = this.order.total_amount;
      this.orderService.getOrderItemById(this.order.id).subscribe(res => {
        this.orderItems = res.data;
        this.orderItems.forEach(element => {
        _.remove(this.products, obj => obj.value === element.item_id);
        });
        this.createForm();
        this.getvendors();
      }, err => {
        this.errorGettingOrders(err);
      });
    }, err => {
      this.errorGettingOrders(err);
    });
  }

  getvendors() {
    const vendors = [];
    this.vendorsService.getVendors().subscribe(res => {
      res.data.forEach(vendor => {
        vendors.push({
          'value': vendor.id,
          'label': vendor.v_name
        });
      });
      this.vendors = vendors;
      this.ngxService.stop();
    });
  }

  /**
   * Method to be called when API causes error in getting orders
   *
   * @param {*} err
   * @memberof OrderDetailComponent
   */
  errorGettingOrders(err) {
    this.toastr.error(err);
    this.router.navigate(['/main/view-orders/']);
    this.ngxService.stop();
  }

  /**
   * This method allows to toggle between edit mode, disable some fields
   *
   * @memberof OrderDetailComponent
   */
  onEditBtn() {
    this.isEdit = !this.isEdit;
  }

  /**
   * This method called when clicked on the delete icon against each product
   *
   * @param {*} id
   * @param {*} new_id
   * @memberof OrderDetailComponent
   */
  deleteOrderItem(id, new_id) {
    const removedOrderItem = _.find(this.orderItems, obj => obj.item_id === id);
    if (new_id === undefined) {
      this.order.total_amount -= removedOrderItem.total_amount;
      // _.remove(this.timeLogsToBeUpdate, obj => obj.id === id);
      this.orderItemsToBeRemoved.push(removedOrderItem);
    } else {
      _.remove(this.orderItemsToBeAdded, obj => obj.new_id === new_id);
      // _.remove(this.timeLogsToBeUpdate, obj => obj.inTime === new_id);
    }
    _.remove(this.orderItems, obj => obj.item_id === id);
    this.products.push({
      'label': removedOrderItem.item_name,
      'value': removedOrderItem.item_id
    });
  }

  /**
   * Method that checks the emptiness of order items in an order
   *
   * @memberof OrderDetailComponent
   */
  emptyOrderItemsValidation() {
    if (this.orderItems.length === 0) {
      this.callEmptyOrderItemsConfirmationPopup();
    } else {
      this.saveTimeTrack();
    }
  }

  /**
   * This method calls the popup for empty order items confirmation
   *
   * @memberof OrderDetailComponent
   */
  callEmptyOrderItemsConfirmationPopup() {
    this.confirmationPopupObject = {
      'msg': 'Saving with ZERO order items will delete the Order. Please Confirm.',
      'cancel': 'Cancel', 'confirm': 'Save Anyway'
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.confirmationPopupObject;
    const modalDialog = this.matDialog.open(ConfirmationPopupComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        this.saveTimeTrack();
      } else {
        this.resetOrderItems();
      }
    });
  }

  /**
   * This method resets the order items when user clicks on the cancel option on
   * confirmation popup
   *
   * @memberof OrderDetailComponent
   */
  resetOrderItems() {
    this.orderItemsToBeRemoved.forEach(orderItem => {
      this.orderItems.push(orderItem);
    });
  }

  /**
   * This method calls the delete order confirmation popup
   *
   * @memberof OrderDetailComponent
   */
  confirmDelete() {
    this.confirmationPopupObject = {
      'msg': 'Are you sure you want to delete this Order?',
      'cancel': 'Cancel', 'confirm': 'Delete'
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.confirmationPopupObject;
    const modalDialog = this.matDialog.open(ConfirmationPopupComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        this.deleteOrder();
      }
    });
  }

  /**
   * This method used to delete the order by also deleting the related order items
   *
   * @memberof OrderDetailComponent
   */
  deleteOrder() {
    this.ngxService.start();
    this.orderService.removeOrder(this.order).subscribe(res => {
      this.orderItems.forEach(orderItem => {
        this.orderService.removeOrderItem(orderItem).subscribe(response => {
        });
        this.updateVendorBalance(true);
        this.successMessageDisplay('Order has been deleted succesfully!', true);
      });
    },
      error => {
        this.ngxService.stop();
        this.toastr.error(error.error_message);
      });
  }

  /**
   * This method saves the order when user click on save button
   *
   * @memberof OrderDetailComponent
   */
  saveTimeTrack() {
    // this.ngxService.start();
    if (this.isChangeFlag === false && this.orderItemsToBeAdded.length === 0
      && this.orderItemsToBeRemoved.length === 0) {
      this.toastr.error('No field is changed!');
      // this.ngxService.stop();
    } else {
      this.updateOrderItems();
    }
  }

  /**
   * This method updates the order items
   *
   * @memberof OrderDetailComponent
   */
  updateOrderItems() {
    // this.ngxService.start();
    this.removeOrderItems([this.addOrderItems.bind(this)]);
  }

  /**
   * Method that removes the order items, send delete request and
   * call the passed callbacks method
   *
   * @param {*} callbacks
   * @memberof OrderDetailComponent
   */
  removeOrderItems(callbacks) {
    if (this.orderItemsToBeRemoved.length > 0) {
      this.orderItemsToBeRemoved.forEach(orderItem => {
        this.orderService.removeOrderItem(orderItem).subscribe(res => {
        });
      });
      if (this.orderItems.length === 0) {
        this.successMessageDisplay('Order has been deleted!', true);
      } else {
        callbacks[0]();
        // callbacks[1]();
      }
    } else {
      callbacks[0]();
      // callbacks[1]();
    }
  }

  /**
   * Method that calls the update order API to update the order
   *
   * @memberof OrderDetailComponent
   */
  updateOrder() {
    this.order.modifiedBy = this.userService.getUser().name;
    this.order.no_of_products = this.orderItems.length;
    this.orderService.updateOrder(this.order).subscribe(res => {
      this.updateVendorBalance(false);
    });
  }

  /**
   * Update the balance of the selected vendor if total amount of order changes
   *
   * @param {*} isOrderDeleted
   * @memberof OrderDetailComponent
   */
  updateVendorBalance(isOrderDeleted) {
    this.vendor.id = this.order.vendor_id;
    this.getAmountDiff(isOrderDeleted);
    this.vendorsService.updateVendorBalance(this.vendor).subscribe(res => {
      this.vendorsService.saveVendorsLocalStorage(null);
      this.ngxService.stop();
      this.successMessageDisplay('Order has been Updated!', false);
    }, err => {
      this.toastr.error(err);
    });
  }

  /**
   * Calculates the differenct of the cuurent and previous total amount of the order
   *
   * @param {*} isOrderDeleted
   * @memberof OrderDetailComponent
   */
  getAmountDiff(isOrderDeleted) {
    if (isOrderDeleted === false) {
      if (this.order.total_amount > this.preAmount) {
        this.preAmount = this.order.total_amount - this.preAmount;
        this.vendor.v_balance = this.preAmount;
        this.vendor.status = 'Added';
      } else {
        this.preAmount = this.preAmount - this.order.total_amount;
        this.vendor.v_balance = this.preAmount;
        this.vendor.status = 'Removed';
      }
    } else {
      this.vendor.v_balance = this.preAmount;
      this.vendor.status = 'Removed';
    }
  }

  /**
   * Method that calls the API to add the order items against the related order
   *
   * @memberof OrderDetailComponent
   */
  addOrderItems() {
    if (this.orderItemsToBeAdded.length > 0) {
      this.orderItemsToBeAdded.forEach(orderItem => {
        const bulkOrderItems = [this.order.id, orderItem.item_id, orderItem.item_name,
                                orderItem.quantity, orderItem.item_price, orderItem.total_amount];
        this.bulkOrderItems.push(bulkOrderItems);
      });
      this.orderService.postOrderItem(this.bulkOrderItems).subscribe(res => {
        this.addItemQuantity();
        this.removeItemQuantity();
        this.updateOrder();
      });
    } else {
      this.updateOrder();
    }
  }

  /**
   * This method is used to display message for successful entry and
   * reroutes the user to orders list view
   *
   * @memberof OrderDetailComponent
   */
  successMessageDisplay(msg, isRemoveOrder) {
    if (isRemoveOrder === true) {
      this.orderService.removeOrder(this.order).subscribe(res => {
        this.toastr.success(msg);
        this.ngxService.stop();
        this.router.navigate(['/main/view-orders/']);
      });
    } else {
      this.toastr.success(msg);
      this.ngxService.stop();
      this.router.navigate(['/main/view-orders/']);
    }
  }

  /**
   * Method that gets called when click on the plus button to add a product
   *
   * @memberof OrderDetailComponent
   */
  addProduct() {
    this.openPopup();
  }

  /**
   * Method that opens the Add a product popup
   *
   * @memberof OrderDetailComponent
   */
  openPopup() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = '75vh';
    dialogConfig.width = '80vw';
    dialogConfig.data = { 'itemsDropdown': this.products,
                          'products': this.items};
    const modalDialog = this.matDialog.open(ProductPopupComponent, dialogConfig);

    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        const product = {
          'order_id': this.order.id, 'item_id': data.item_id, 'item_price': data.item_price,
          'item_name': data.item_name, 'quantity': data.quantity, 'total_amount': data.total_amount,
          'new_id': data.item_id + 1
        };
        this.order.total_amount += product.total_amount;
        this.orderItems.push(product);
        this.orderItemsToBeAdded.push(product);
      }
    });
  }

  /**
   * Called when city is selected
   *
   * @param {*} event
   * @memberof OrderDetailComponent
   */
  cityIsSelected(event) {
    this.isChangeFlag = true;
    this.order.city = event.label;
  }

  /**
   * Called when order type is selected
   *
   * @param {*} event
   * @memberof OrderDetailComponent
   */
  orderTypeIsSelected(event) {
    this.isChangeFlag = true;
    this.order.order_type = event.label;
  }

  /**
   * Called when status is selected
   *
   * @param {*} event
   * @memberof OrderDetailComponent
   */
  statusIsSelected(event) {
    this.isChangeFlag = true;
    this.order.status = event.label;
  }

  /**
   * This method is used to add to the product's inventory
   *
   * @memberof OrderDetailComponent
   */
  addItemQuantity() {
    const status = 'Added';
    if (this.orderItemsToBeAdded.length > 0) {
      this.orderItemsToBeAdded.forEach(item => {
        this.itemService.updateItemQuantity(item.id, item.quantity, status).subscribe(res => {
          this.itemService.saveItemsLocalStorage(null);
        }, err => {
          this.toastr.error('Please repeat your action item due to this error:' + err);
          this.ngxService.stop();
        });
      });
      this.orderItemsToBeAdded = [];
    }
  }

  /**
   *  This method is used to subtract to the product's inventory
   *
   * @memberof OrderDetailComponent
   */
  removeItemQuantity() {
    const status = 'Removed';
    if (this.orderItemsToBeRemoved.length > 0) {
      this.orderItemsToBeRemoved.forEach(item => {
        this.itemService.updateItemQuantity(item.id, item.quantity, status).subscribe(res => {
          this.itemService.saveItemsLocalStorage(null);
        }, err => {
          this.toastr.error('Please repeat your action item due to this error:' + err);
          this.ngxService.stop();
        });
      });
      this.orderItemsToBeRemoved = [];
    }
  }

}
