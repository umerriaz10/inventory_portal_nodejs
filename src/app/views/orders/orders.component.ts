import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IOption } from 'ng-select';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { Items } from 'src/app/interfaces/items';
import { ItemsService } from 'src/app/services/items/items.service';
import { VendorsService } from 'src/app/services/vendors/vendors.service';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user/user.service';
import { Vendors } from 'src/app/interfaces/vendors';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof RegisterComponent
   */
  simpleForm: FormGroup;

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  vendors: Array<IOption> = [];

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('myDateInput', {static: false}) myDateInput: jqxDateTimeInputComponent;

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  cities: Array<IOption> = [{
    'value': '1',
    'label': 'LHR'
  }, {
    'value': '2',
    'label': 'RWP'
  }, {
    'value': '3',
    'label': 'PEW'
  }];

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  statuses: Array<IOption> = [{
    'value': '1',
    'label': 'Pending'
  }, {
    'value': '2',
    'label': 'Delivered'
  }, {
    'value': '3',
    'label': 'Paid'
  }];

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  orderTypes: Array<IOption> = [{
    'value': '1',
    'label': 'Cash'
  }, {
    'value': '2',
    'label': 'Credit'
  }];

  /**
   * This array holds the options for countries dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RegisterComponent
   */
  dropDownProducts: Array<IOption> = [];

  /**
   * This array is used to store the selected products values
   *
   * @type {Array<any>}
   * @memberof RegisterComponent
   */
  products: Array<any> = [];

  /**
   * This variable checks when the add button should be make disable
   *
   * @memberof RegisterComponent
   */
  addBtnDisable = true;

  /**
   * This variable is used to store the link added by user for currently selected social media
   *
   * @memberof RegisterComponent
   */
  selectedProductQuantity = 0;

  /**
   * This variable is used to store the link added by user for currently selected social media
   *
   * @memberof RegisterComponent
   */
  selectedDropdownPrice = 0;

  /**
   * This variable is used to store the value of currently selected social media
   *
   * @memberof RegisterComponent
   */
  selectedProductID = '';

  /**
   * This array is used to store the selected products values
   *
   * @type {Array<any>}
   * @memberof RegisterComponent
   */
  selectedProducts: Array<any> = [];

  /**
   * holds the bulk order items
   *
   * @type {InventoryRecord}
   * @memberof OrderDetailComponent
   */
  bulkOrderItems: Array<any> = [];

  /**
   * This variable is used to set the max date in the datepicker
   *
   * @memberof OrderDetailComponent
   */
  maxDate = new Date();

  /**
   * Holds the date that user selects
   *
   * @memberof OrderDetailComponent
   */
  selectedDate = new Date();

  /**
   * This object is used to hold the order values
   *
   * @type {<any>}
   * @memberof RegisterComponent
   */
  order: Order = {};

  totalBill = 0;

  vendor: Vendors = {};

  isValid = false;

  isAdded = false;

  submitted = false;

  itemAvaiableQuantity = 0;

  isQuanErr = false;

  quanErrMsg = '';

  priceErrMsg = '';

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private itemService: ItemsService,
    private vendorsService: VendorsService,
    private orderService: OrdersService,
    private userService: UserService,
    private ngxService: NgxUiLoaderService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getProducts();
    this.getvendors();
  }

  /**
   * This method creates the form with all the relevant controls and validations.
   *
   * @memberof RegisterComponent
   */
  createForm() {
    this.simpleForm = this.formBuilder.group({
      vendor: [null, [Validators.required]],
      city: [null, [Validators.required]],
      status: [null, [Validators.required]],
      orderType: [null, [Validators.required]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  getProducts() {
    const products = [];
    this.itemService.getItems().subscribe(res => {
      this.products = res.data;
      res.data.forEach(item => {
        products.push({
          'value': item.id,
          'label': item.p_name
        });
      });
      this.dropDownProducts = products;
    });
  }

  getvendors() {
    const vendors = [];
    this.vendorsService.getVendors().subscribe(res => {
      res.data.forEach(vendor => {
        vendors.push({
          'value': vendor.id,
          'label': vendor.v_name
        });
      });
      this.vendors = vendors;
    });
  }

  /**
   * This method is used to delete a social media row that is selected by the user
   *
   * @param option
   * @memberof RegisterComponent
   */
  deleteEntry(option) {
    const product = _.filter(this.selectedProducts, obj => obj.id === option.id)[0];
    this.totalBill -= product.total;
    _.remove(this.selectedProducts, obj => obj.id === option.id);
    this.dropDownProducts.push({
      'label': product.name,
      'value': product.id
    });
    this.dropDownProducts = [...this.dropDownProducts];
  }

  /**
   * This method is triggered when user clicks on any of the dropdowns in 'country', 'state', and 'city'
   * in order to keep the dropdown from opening upwards
   *
   * @param value
   * @memberof RegisterComponent
   */
  scrollOnDropdownExpansion(value) {
    window.scrollTo(0, value);
  }

  /**
   * This method is used to set ID of the product that is selected by the user
   *
   * @param event
   * @memberof RegisterComponent
   */
  setSelectedProductID(event) {
    if (event !== undefined) {
      this.isAdded = true;
      const ddProduct = _.filter(this.products, obj => obj.id === event.value)[0];
      this.selectedDropdownPrice = ddProduct.p_price;
      this.itemAvaiableQuantity = ddProduct.p_quantity;
      this.selectedProductID = event.value;
      if (this.selectedProductQuantity !== 0) {
        this.addBtnDisable = false;
      }
    } else {
      this.isAdded = false;
      this.selectedProductQuantity = this.selectedDropdownPrice = 0;
      this.selectedProductID = '';
      this.addBtnDisable = true;
    }
  }

  /**
   * This method is used to set the link entered by the user (triggered on keyup in input)
   *
   * @param event
   * @memberof RegisterComponent
   */
  setSelectedProductQuantity(event) {
    this.selectedProductQuantity = 0;
    this.addBtnDisable = true;
    this.isQuanErr = true;
    const quantity = event.target.value;
    if (quantity !== 0 && quantity > 0 && quantity < this.itemAvaiableQuantity) {
      this.selectedProductQuantity = quantity;
      this.addBtnDisable = false;
      this.isQuanErr = false;
    } else if (quantity < 0) {
      this.quanErrMsg = 'Enter a positive Quantity';
      this.toastr.warning('Quantity Should be a positive number');
    } else if (quantity > this.itemAvaiableQuantity) {
      this.toastr.warning('Please enter Quantity less than the Available Quantity');
      this.quanErrMsg = 'Enter a valid Quantity';
    } else {
      this.quanErrMsg = 'Quantity is Required';
    }
  }

  /**
   * This method is used to set the link entered by the user (triggered on keyup in input)
   *
   * @param event
   * @memberof RegisterComponent
   */
  setSelectedProductPrice(event) {
    this.addBtnDisable = true;
    this.selectedDropdownPrice = 0;
    const price = event.target.value;
    if (price !== 0 && price > 0) {
      this.selectedDropdownPrice = price;
      this.addBtnDisable = false;
      this.priceErrMsg = '';
    } else if (price < 0) {
      this.priceErrMsg = 'Enter a positive Price';
      this.toastr.warning('Price Should be a positive number');
    } else {
      this.priceErrMsg = 'Enter a valid Price';
    }
  }

  /**
   * This method is used to validate and add the selected social media value, and link in the selectedSocialMedia Array,
   * and reset DOM elements
   *
   * @returns
   * @memberof RegisterComponent
   */
  selectProduct() {
    if (this.selectedProductID === '' || this.selectedProductQuantity === 0 || this.selectedDropdownPrice === 0) {
      this.toastr.error('Please enter valid input for Product');
      return;
    }
    const price = this.selectedDropdownPrice;
    this.selectedProducts.push({
      'id': this.selectedProductID,
      'name': _.filter(this.dropDownProducts, obj => obj.value === this.selectedProductID)[0].label,
      'quantity': this.selectedProductQuantity,
      'price': price,
      'total': this.selectedProductQuantity * price
    });

    this.totalBill += this.selectedProductQuantity * price;

    _.remove(this.dropDownProducts, obj => obj.value === this.selectedProductID);
    this.dropDownProducts = [...this.dropDownProducts];

    this.selectedProductID = '';
    this.selectedProductQuantity = this.selectedDropdownPrice = 0;
    this.addBtnDisable = true;
    this.isAdded = false;
  }

  placeOrder() {
    this.orderService.postOrder(this.order).subscribe(res => {
      const orderId = res.data.insertId;
      this.selectedProducts.forEach(orderItem => {
        const bulkOrderItems = [orderId, orderItem.id, orderItem.name, orderItem.quantity, orderItem.price, orderItem.total];
        this.bulkOrderItems.push(bulkOrderItems);
      });
      this.orderService.postOrderItem(this.bulkOrderItems).subscribe(response => {
        this.updateVendorBalance();
        });
    }, err => {
      this.toastr.error("dsadsads");
    });
  }

  checkFormValidation() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.simpleForm.invalid) {
      this.toastr.error('Please Check Your Form Fields!');
      this.ngxService.stop();
      this.isValid = false;
    }
    if (this.isValid === true) {
      if (this.selectedProducts.length > 0) {
      this.setValues();
      } else {
        this.toastr.error('Please ADD some products for the Order');
      }
    }
  }

  setValues() {
    const form = this.simpleForm.controls;
    this.order.date = this.selectedDate;
    this.order.status = 'Pending';
    this.order.orderedBy = this.userService.getUser().name;
    this.order.no_of_products = this.selectedProducts.length;
    this.order.total_amount = this.totalBill;
    this.order.vendor_name = form.vendor.value.label;
    this.order.vendor_id = form.vendor.value.value;
    this.order.city = form.city.value.label;
    this.order.order_type = form.orderType.value.label;
    this.order.status = form.orderType.value.label;
    this.placeOrder();
  }

  updateVendorBalance() {
    this.vendor.id = this.order.vendor_id;
    this.vendor.v_balance = this.order.total_amount;
    this.vendor.status = 'Added';
    this.vendorsService.updateVendorBalance(this.vendor).subscribe(res => {
      this.vendorsService.saveVendorsLocalStorage(null);
      this.updateItem();
      this.ngxService.stop();
      this.toastr.success('Order has been created successfully!');
    }, err => {
      this.toastr.error(err);
    });
  }

  /**
   * This method is used to update the products inventory
   */
  updateItem() {
    const status = 'Added';
    this.selectedProducts.forEach(item => {
      this.itemService.updateItemQuantity(item.id, item.quantity, status).subscribe(res => {
        this.itemService.saveItemsLocalStorage(null);
      }, err => {
        this.toastr.error('Please repeat your action item due to this error:' + err);
        this.ngxService.stop();
      });
    });
    this.selectedProducts = [];
  }

}
