import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as _ from 'lodash';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-responsive';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { Order } from 'src/app/interfaces/order';
import { ModalServiceService } from 'src/app/services/modalService/modal-service.service';
import { Router } from '@angular/router';
import { Payment } from 'src/app/interfaces/payment';
import { PaymentsService } from 'src/app/services/payments/payments.service';
import { ViewPaymentPopupComponent } from 'src/app/components/view-payment-popup/view-payment-popup.component';
import { ConfirmationPopupComponent } from 'src/app/components/confirmation-popup/confirmation-popup.component';
import { VendorsService } from 'src/app/services/vendors/vendors.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
/**
   * This variable is used to set configuration of the modal.
   *
   * @memberof TimeLogPopUpComponent
   */
  config = {
    backdrop: false,
    ignoreBackdropClick: false,
    keyboard: false
  };

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  payments: Array<Payment> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  receivedPayments: Array<Payment> = [];

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<Orders>}
   * @memberof InventoryHistoryComponent
   */
  paidPayments: Array<Payment> = [];

  @ViewChild('receivedDataTable', { static: true }) receivedTable;

  @ViewChild('paidDataTable', { static: true }) paidTable;

  receivedTableRow;

  paidTableRow;

  receivedDataTable: any;

  paidDataTable: any;

  dtOption = {
    'paging':   false,
    'ordering': true,
    'info':     false,
    'responsive': {
      'details': true,
      'type': 'inline'
    }
  };

  order: Order = {};

  isButtonClicked = false;

  /**
   * Object for defining confirmation popup attributes
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  confirmationPopupObject: any = {
    'msg': '',
    'cancel': '',
    'confirm': ''
  };

  constructor(
    private ngxService: NgxUiLoaderService,
    private paymentService: PaymentsService,
    private vendorService: VendorsService,
    private toastr: ToastrService,
    public matDialog: MatDialog,
    private router: Router,
    private orderService: OrdersService,
    private modalService: ModalServiceService
  ) { }

  /**
   * This method is triggered when the component is initialized and get all the records
   *
   * @memberof InventoryHistoryComponent
   */
  ngOnInit() {
    this.getAllPayments();
  }

  /**
   * Get all the inventory records from firestore
   *
   * @memberof InventoryHistoryComponent
   */
  getAllPayments() {
    this.ngxService.start();
    this.payments = [];
    this.receivedDataTable = $(this.receivedTable.nativeElement);
    this.paidDataTable = $(this.paidTable.nativeElement);
    this.receivedDataTable.DataTable().destroy();
    this.paidDataTable.DataTable().destroy();
    const payments = this.paymentService.getPaymentsFromLocal();
    if (payments === undefined || payments === null) {
    this.paymentService.getPayments().subscribe(res => {
      if (res.data) {
        res.data.forEach(rec => {
          this.payments.push({
            'id': rec.id,
            'date': rec.date,
            'vendor_id': rec.vendor_id,
            'vendor_name': rec.vendor_name,
            'order_id': rec.order_id,
            'amount': rec.amount,
            'pay': rec.pay,
            'receive': rec.receive,
            'addedBy': rec.addedBy
          });
        });
        this.dividePayments();
      }
    }, err => {
      this.toastr.error(err);
      this.ngxService.stop();
    });
  } else {
    this.payments = payments;
    this.dividePayments();
  }
  }

  refreshRecords() {
    this.paymentService.savePaymentsLocalStorage(null);
    this.getAllPayments();
  }

  dividePayments() {
    this.receivedPayments = _.filter(this.payments, obj => obj.receive === 1);
    this.paidPayments = _.filter(this.payments, obj => obj.pay === 1);
    this.setReceivedPaymentsDataTables();
    this.setPaidOrdersDataTables();
    this.ngxService.stop();
  }

  setReceivedPaymentsDataTables() {
    this.receivedTableRow = this.receivedDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.receivedPayments,
      columns: [
        { 'data': 'date',
          'defaultContent': ''},
        { 'data': 'vendor_name' },
        { 'data': 'amount' },
        { 'data': 'addedBy' },
        { 'data': null,
          'defaultContent': '<button class="btn btn-secondary mr-1 expense-button">Delete</button>'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details',
      }});
      this.viewPayments(this.receivedDataTable, this.receivedTableRow);
  }

  setPaidOrdersDataTables() {
    this.paidTableRow = this.paidDataTable.DataTable({
      dtOption: this.dtOption,
      data: this.paidPayments,
      columns: [
        { 'data': 'date',
          'defaultContent': ''},
        { 'data': 'vendor_name' },
        { 'data': 'amount' },
        { 'data': 'addedBy' },
        { 'data': null,
          'defaultContent': '<button class="btn btn-secondary mr-1 expense-button">Delete</button>'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details',
      }});
      this.viewPayments(this.paidDataTable, this.paidTableRow);
  }

  viewPayments(dataTable, tableRow) {
    const self = this;
    $(dataTable).on( 'click', 'button', function () {
      self.isButtonClicked = true;
      const data = tableRow.row( $(this).parents('tr') ).data();
      self.confirmDelete(data);
    });
    $(dataTable).on( 'click', 'tr', function (e) {
      const data = tableRow.row(this).data();
      if (self.isButtonClicked === true) {
        e.stopPropagation();
        self.isButtonClicked = false;
      } else {
      if (data.pay === 1) {
        self.openPaidPaymentPopup(data);
      } else {
        self.openReceivedPaymentPopup(data);
      }
    }
  });
  }

  /**
   * This method calls the delete order confirmation popup
   *
   * @memberof OrderDetailComponent
   */
  confirmDelete(paymentObject) {
    this.confirmationPopupObject = {
      'msg': 'Are you sure you want to delete this Payment?',
      'cancel': 'Cancel', 'confirm': 'Delete'
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.confirmationPopupObject;
    const modalDialog = this.matDialog.open(ConfirmationPopupComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        this.deletePayment(paymentObject);
      }
    });
  }

  openPaidPaymentPopup(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = 'auto';
    dialogConfig.data = {'data': data, 'paymentType': 'Paid Payment', 'order': null};
    const modalDialog = this.matDialog.open(ViewPaymentPopupComponent, dialogConfig);
   }

   openReceivedPaymentPopup(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = 'auto';
     if (data.order_id !== null) {
       this.orderService.getOrderById(data.order_id).subscribe(res => {
         this.order = res.data[0];
         dialogConfig.data = {'data': data, 'paymentType': 'Paid Payment', 'order': this.order};
    const modalDialog = this.matDialog.open(ViewPaymentPopupComponent, dialogConfig);
       }, err => {
        this.toastr.error(err);
       });
     } else {
       this.order = null;
       dialogConfig.data = {'data': data, 'paymentType': 'Paid Payment', 'order': this.order};
        const modalDialog = this.matDialog.open(ViewPaymentPopupComponent, dialogConfig);
     }
   }

   deletePayment(data) {
     this.ngxService.start();
      this.vendorService.getVendorById(data.vendor_id).subscribe(res => {
        const vendor = res.data[0];
        vendor.v_balance = vendor.v_balance + data.amount;
        vendor.v_totalPaid = vendor.v_totalPaid - data.amount;
        this.updateVendor(vendor);
        this.updatePayment(data);
      }, err => {
        this.ngxService.stop();
        this.toastr.error('Please repeat your action due to this error: ' + err);
      });
   }

   updateVendor(vendor) {
    this.vendorService.updateVendor(vendor).subscribe(res => {
      this.vendorService.saveVendorsLocalStorage(null);
    }, err => {
      this.ngxService.stop();
      this.toastr.error('Please repeat your action due to this error: ' + err);
    });
   }

   updatePayment(payment) {
    this.paymentService.removePayment(payment).subscribe(res => {
      if (payment.order_id !== null) {
        this.toastr.warning('Please update the Status of the linked Order');
      }
      this.ngxService.stop();
      this.paidDataTable.DataTable().destroy();
      this.receivedDataTable.DataTable().destroy();
      this.payments = [];
      this.getAllPayments();
    }, err => {
      this.ngxService.stop();
      this.toastr.error('Please repeat your action due to this error: ' + err);
    });
   }
}
