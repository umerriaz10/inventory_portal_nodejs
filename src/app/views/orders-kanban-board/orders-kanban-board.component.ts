import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy} from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { jqxKanbanComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxkanban';
import * as moment from 'moment';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders/orders.service';

@Component({
  selector: 'app-orders-kanban-board',
  templateUrl: './orders-kanban-board.component.html',
  styleUrls: ['./orders-kanban-board.component.scss']
})
export class OrdersKanbanBoardComponent implements OnInit {

  public topHeadHeight: any;
  public mainWindow: any ;
  public kanbanMain: any;

  /**
   * This variable is used to store a list of all projects assigned to the freelancer.
   *
   * @memberof KanbanBoardComponent
   */
  orders: Array<Order>;


  prevProjects: Array<any>;

  /**
   * Reference to the Kanban element
   *
   * @type {jqxKanbanComponent}
   * @memberof KanbanBoardComponent
   */
  @ViewChild('myKanbanThree', { static: true }) myKanbanThree: jqxKanbanComponent;

  /**
   * Array that holds the data from projects
   *
   * @type {*}
   * @memberof KanbanBoardComponent
   */
  kanbanData: any = [];

  /**
   * This holds the full data source for kanban view
   *
   * @type {*}
   * @memberof KanbanBoardComponent
   */
  source: any;

  /**
   * This holds the full data source for kanban view
   *
   * @type {*}
   * @memberof KanbanBoardComponent
   */
  clickedItem: {
    id: '',
    tags: '',
    content: '',
    text: '',
    resourceId: '',
    status: null
  };

  /**
   * DataAdaptar for kanban view
   *
   * @type {*}
   * @memberof KanbanBoardComponent
   */
  dataAdapter: any;

  /**
   * This holds the maximum items for a column
   *
   * @type {*}
   * @memberof KanbanBoardComponent
   */
  maxProjects: any;

  /**
   * This holds the maximum items for a column
   *
   * @type {*}
   * @memberof KanbanBoardComponent
   */
  allProjectsSet = false;


  /**
   * This holds the kanban columns Data
   *
   * @type {any[]}
   * @memberof KanbanBoardComponent
   */
  kanbanThreeColumns: any[] =
    [
      { text: 'Pending', dataField: 'Pending', color: '#F44336' },
      { text: 'Delivered', dataField: 'Delivered', color: '#8BC34A' },
      { text: 'Paid', dataField: 'Paid', color: '#009688' }
    ];

  template: string =
    '<div class="jqx-kanban-item" id="">'
    + '<div class="jqx-kanban-item-color-status" style="background-color: rgb(23, 162, 184);"></div>'
    + '<div style="display: none;" class="jqx-kanban-item-avatar"></div>'
    + '<div class="jqx-kanban-item-text"></div>'
    + '<div class="jqx-kanban-item-footer"></div>'
    + '<div class="jqx-kanban-clearing"></div>'
    + '</div>';

   /**
   * This variable is used to store the start time
   *
   * @memberof PositionDetailComponent
   */
  startDate;

 /**
   * This variable is used to store the end time
   *
   * @memberof PositionDetailComponent
   */
  endDate;

  /**
   * property bind to the jqxkanban columnRenderer that is called on each column rendering
   * and calculates the total item for each column
   * @type {*}
   * @memberof DashboardMainComponent
   */
  kanbanThreeColumnRenderer: any = (element: any, collapsedElement: any, column: any): void => {
    if (element[0]) {
      const columnItems = this.myKanbanThree.getColumnItems(column.dataField).length;
      const headerStatus = element[0].getElementsByClassName('jqx-kanban-column-header-status')[0];
      headerStatus.innerHTML = ' (' + columnItems + '/' + column.maxItems + ')';
      const collapsedHeaderStatus = collapsedElement[0].getElementsByClassName('jqx-kanban-column-header-status')[0];
      collapsedHeaderStatus.innerHTML = ' (' + columnItems + '/' + column.maxItems + ')';
    }
  }
  /**
   * Creates an instance of KanbanBoardComponent, and calls method for sanitizing 'allProjects' Array.
   * @param {ProjectService} projectService
   * @param {NgxUiLoaderService} ngxService
   * @param {ToastrService} toastr
   * @param {UserService} UserService
   * @param {Router} router
   * @memberof KanbanBoardComponent
   */
  constructor(
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private orderService: OrdersService,
    private userService: UserService,
    private router: Router) { }

  /**
   * This method is used by Angular lifecycle hooks.
   *
   * This method is used to call the 'initializeProductionProjectsWidgets' method for initializing the widgets
   * of projects fetched from Sugar.
   *
   * @memberof KanbanBoardComponent
   */
  ngOnInit() {
    this.ngxService.start();
    this.initializeProductionProjectsWidgets();
  }

  /**
   * property bind to the jqxkanban itemRenderer that is called on each item rendering
   * and stop the mouse down event for all items to prevent dragging
   *
   * @type {*}
   * @memberof KanbanBoardComponent
   */
  itemRenderer: any = (element): void => {
    if (element[0]) {
      element[0].addEventListener('click', _.bind(function(e) {
        this.bindItemEventsOnClick (e, element[0]);
      }, this));
      element[0].addEventListener('touch', _.bind(function(e) {
        this.bindItemEventsOnClick (e, element[0]);
      }, this));
      element[0].addEventListener('mousedown', function(e) {
        e.stopPropagation();
      });
      element[0].addEventListener('touchstart', function(e) {
        e.stopPropagation();
      });
    }
  }

  bindItemEventsOnClick (event, element) {
     if (!_.isEmpty(this.clickedItem)) {
      const id = this.clickedItem.id.split('_')['0'];
      const vendorName = this.clickedItem.text;
      this.router.navigate([`main/kanban-board/order-detail/${vendorName}/` + id]);

    }
  }
  // getProjectByFilter (id, position) {
  //   return this.allProjects.filter(x => (x.project_id === id && x.new_position_name === position));
  // }

  /**
   * This method initializes the widgets for Production and Install Projects.
   *
   * It santizes the retrieved projects' data by formatting names, appending their respective modules
   * & classes.
   *
   * @memberof KanbanBoardComponent
   */
  initializeProductionProjectsWidgets() {
    const orders = this.orderService.getOrdersFromLocal();
    if (orders === null) {
      this.orderService.getOrders().subscribe(res => {
        // if (response !== null) {
          // this.projectService.setAllProjectsWithKanbanStages(response);
          // this.allProjects =  this.projectService.getKanbanProjects();
          this.orders = res.data;
          this.setKanbanData(this.orders);
          this.allProjectsSet = true;
          this.ngxService.stop();
      });
    } else if (orders.length === 0) {
      this.allProjectsSet = false;
      this.ngxService.stop();
    } else {
      this.orders = orders;
      this.setKanbanData(this.orders);
      this.allProjectsSet = true;
      this.ngxService.stop();
      setTimeout(function() {
        this.topHeadHeight = document.querySelector('.custom-breadcrumb.breadcrumb').clientHeight + 50;
        this.mainWindow = document.querySelector('.main.main-window').clientHeight - this.topHeadHeight;
        this.kanbanMain = document.querySelector('app-kanban-board');
        this.mainWindow =  this.mainWindow + 'px';
        this.kanbanMain.style.height = this.mainWindow;
      }, 2000);
    }
  }

  /*
   * This method is used to refresh the projects data stored in cache.
   *
   * @memberof KanbanBoardComponent
   */
  refreshProjects() {
    this.orders = null;
    this.kanbanData = [];
    this.ngxService.start();
    this.orderService.saveOrdersLocalStorage(null);
    this.initializeProductionProjectsWidgets();
  }

  /**
   * Sets the data for kanban view from projects
   * @param {*} projects
   * @memberof KanbanBoardComponent
   */
  setKanbanData(projects) {
    this.maxProjects = projects.length;
    projects.forEach(element => {
      this.kanbanData.push({
        'state' : element.status,
        'project_id' : element.id + '_' + element.vendor_name,
        'label' : element.vendor_name,
        'position' : element.date + ' - ' + element.city + ' - ' + element.order_type,
        'date' : element.date,
        'hex' : 'red',
        'draggable' : false
      });
    });
    this.setKanbanColumnMaxItems();
    this.setKanbanDataSource();
  }

  /**
   * Sets the maximum item for kanban column header
   *
   * @memberof KanbanBoardComponent
   */
  setKanbanColumnMaxItems() {
    this.kanbanThreeColumns.forEach(element => {
      element['maxItems'] = this.maxProjects;
    });
  }

  /**
   * Sets the full data source for kanban view
   *
   * @memberof KanbanBoardComponent
   */
  setKanbanDataSource() {
    this.source = {
      dataType: 'array',
      dataFields: [{
          name: 'status',
          map: 'state',
          type: 'string'
        },
        {
          name: 'id',
          map: 'project_id',
          type: 'string'
        },
        {
          name: 'text',
          map: 'label',
          type: 'string'
        },
        {
          name: 'tags',
          map: 'position',
          type: 'string'
        },
        {
          name: 'date',
          type: 'string'
        },
        {
          name: 'color',
          map: 'hex',
          type: 'string'
        },
        {
          name: 'draggable',
          map: 'draggable',
          type: 'bool'
        }
      ],
      localData: this.kanbanData
    };
    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.ngxService.stop();
  }

  /**
   * Method called on the click of any widget's item and navigate to the respective
   * projects's detail page
   *
   * @param {*} event
   * @memberof KanbanBoardComponent
   */
  itemAttrClicked(event) {
    this.clickedItem = event.args.item;
  }

}
