import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersKanbanBoardComponent } from './orders-kanban-board.component';

describe('OrdersKanbanBoardComponent', () => {
  let component: OrdersKanbanBoardComponent;
  let fixture: ComponentFixture<OrdersKanbanBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersKanbanBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersKanbanBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
