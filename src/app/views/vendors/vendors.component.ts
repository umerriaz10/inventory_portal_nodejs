import { Component, OnInit, ViewChild } from '@angular/core';
import { Vendors } from 'src/app/interfaces/vendors';
import { VendorsService } from 'src/app/services/vendors/vendors.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import 'datatables.net';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddVendorPopupComponent } from 'src/app/components/add-vendor-popup/add-vendor-popup.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent implements OnInit {
/**
   * Holds the items coming from DB
   *
   * @type {Array<Items>}
   * @memberof ProductsComponent
   */
  vendors: Array<Vendors> = [];

  /**
   * Holds the items for data table
   *
   * @type {Array<Items>}
   * @memberof ProductsComponent
   */
  vendorsDatatable: Array<Vendors> = [];

  @ViewChild('dataTable', { static: true }) table;

  /**
   * Holds the datatable object
   *
   * @type {*}
   * @memberof ProductsComponent
   */
  dataTable: any;

  /**
   * Holds the datatable options
   *
   * @memberof ProductsComponent
   */
  dtOption = {
    'paging': false,
    'ordering': true,
    'info': false
  };

  tableRow;

  /**
   * Creates an instance of ProductsComponent.
   *
   * @param {ItemsService} itemService
   * @param {NgxUiLoaderService} ngxService
   * @memberof ProductsComponent
   */
  constructor(
    private vendorService: VendorsService,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private router: Router,
    public matDialog: MatDialog
  ) { }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof ProductsComponent
   */
  ngOnInit() {
    this.dataTable = $(this.table.nativeElement);
    this.getVendors(this.setDataTable.bind(this), false);
  }

  getVendors(callBack, isDestroy) {
    this.ngxService.start();
    const vendors = this.vendorService.getVendorsFromLocal();
    if (vendors === undefined || vendors === null) {
      this.vendorService.getVendors().subscribe(res => {
        this.vendorService.saveVendorsLocalStorage(res.data);
        this.vendors = res.data;
        if (isDestroy === false) {
          callBack();
        } else {
          this.dataTable.DataTable().destroy();
          callBack();
        }
      }, err => {
        this.toastr.error(err);
      });
    } else {
      this.vendors = vendors;
      if (callBack !== null) {
        callBack();
      } else {
        this.dataTable.DataTable().destroy();
        callBack();
      }
    }
  }

  refreshRecords () {
    this.vendorService.saveVendorsLocalStorage(null);
    this.getVendors(this.setDataTable.bind(this), true);
  }

  /**
   * Sets the data table for products
   *
   * @param {*} vendors
   * @memberof ProductsComponent
   */
  setDataTable() {
    this.vendorsDatatable = [];
    this.vendors.forEach(element => {
      this.vendorsDatatable.push({
        'id': element.id,
        'v_name': element.v_name,
        'v_contact_number': element.v_contact_number,
        'v_address': element.v_address,
        'v_balance': element.v_balance,
        'v_totalPaid': element.v_totalPaid,
        'v_paidToVendor': element.v_paidToVendor,
        'v_addedBy': element.v_addedBy
      });
    });
    this.tableRow = this.dataTable.DataTable({
      dtOption: this.dtOption,
      data: this.vendorsDatatable,
      columns: [
        { 'data': 'v_name' },
        { 'data': 'v_contact_number',
        'defaultContent': '-' },
        { 'data': 'v_balance',
        'defaultContent': 0},
        { 'data': 'v_totalPaid',
        'defaultContent': 0},
        { 'data': 'v_paidToVendor',
        'defaultContent': 0},
        { 'data': 'v_addedBy'}
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details/Edit the record',
      }});
      this.viewVendor();
    this.ngxService.stop();
  }

  viewVendor() {
    const self = this;
   $(self.dataTable).on( 'click', 'tr', function () {
     const data = self.tableRow.row(this).data();
     self.router.navigate(['main/view-vendors/vendor-detail/' + data.v_name + '/' + data.id]);
 });
 }

  /**
   * Method that opens the Add a product popup
   *
   * @memberof OrderDetailComponent
   */
  openAddVendorPopup() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = 'auto';
    dialogConfig.data = {'function': this.getVendors.bind(this, this.setDataTable.bind(this), true)};
    const modalDialog = this.matDialog.open(AddVendorPopupComponent, dialogConfig);
  }

}
