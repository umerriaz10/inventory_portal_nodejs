/**
 * This interface is used to create customized breadcrumbs in the portal.
 *
 * @export
 * @interface CustomBreadcrumbs
 */
export interface CustomBreadcrumbs {
    title?: string;
    link?: string;
}
