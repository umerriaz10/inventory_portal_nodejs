export interface OrderItmes {
    order_id?: number;
    item_name?: string;
    item_id?: number;
    quantity?: number;
    item_price?: Date;
    new_id?: number;
}
