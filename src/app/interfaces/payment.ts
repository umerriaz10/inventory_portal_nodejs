export interface Payment {
    id?: number;
    date?: Date;
    vendor_name?: string;
    vendor_id?: number;
    order_id?: number;
    amount?: number;
    pay?: boolean;
    receive?: boolean;
    addedBy?: string;
}
