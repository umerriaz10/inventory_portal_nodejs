/**
 * @ignore
 */
interface NavAttributes {
    [propName: string]: any;
  }

  /**
   * @ignore
   */
interface NavWrapper {
    attributes: NavAttributes;
    element: string;
  }

  /**
   * @ignore
   */
interface NavBadge {
    text: string;
    variant: string;
  }

  /**
   * @ignore
   */
interface NavLabel {
    class?: string;
    variant: string;
  }

  /**
   * @ignore
   */
export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: NavBadge;
    title?: boolean;
    children?: NavData[];
    variant?: string;
    attributes?: NavAttributes;
    divider?: boolean;
    class?: string;
    label?: NavLabel;
    wrapper?: NavWrapper;
  }

  /**
   * @ignore
   */
export const navItems: NavData[] = [
    {
      name: 'Dashboard',
      url: '/main/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Orders',
      url: '/order',
      icon: 'icon-calendar',
      children: [
        {
          name: 'New Order',
          url: '/main/new-order',
          icon: 'fa fa-clock-o',
        },
        {
          name: 'View Order',
          url: '/main/view-orders',
          icon: 'fa fa-user-times',
        }
      ]
    },
    {
      name: 'Payments',
      url: '/payment',
      icon: 'icon-calendar',
      children: [
        {
          name: 'New Payment',
          url: '/main/new-payment',
          icon: 'fa fa-clock-o',
        },
        {
          name: 'View Payments',
          url: '/main/payments',
          icon: 'fa fa-user-times',
        }
      ]
    },
    {
      name: 'Vendors',
      url: '/main/view-vendors',
      icon: 'icon-note',
    },
    {
      name: 'Products',
      url: '/main/products',
      icon: 'icon-note',
    },
    {
      name: 'Iventory Records',
      url: '/main/inventory-records',
      icon: 'icon-note',
    },
    {
      name: 'Kanban Board',
      url: '/main/kanban-board',
      icon: 'icon-note',
    },
    {
      name: 'Company Summary',
      url: '/main/company-summary',
      icon: 'icon-note',
    },
    {
      name: 'Logout',
      url: 'logout',
      icon: 'icon-logout',
    }
  ];
