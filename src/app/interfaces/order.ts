/**
 * This holds the model of database order
 *
 * @export
 * @interface Order
 */
export interface Order {
    id?: number;
    vendor_name?: string;
    vendor_id?: number;
    no_of_products?: number;
    date?: Date;
    city?: string;
    status?: string;
    order_type?: string;
    orderedBy?: string;
    modifiedBy?: string;
    total_amount?: number;
}
