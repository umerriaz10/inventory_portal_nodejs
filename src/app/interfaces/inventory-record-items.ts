/**
 * This interface is used to store inverntory records.
 *
 * @export
 * @interface InvertoryRecordItems
 */

export interface InventoryRecordItems {
    invRec_id?: number;
    item_id?: number;
    item_name?: Date;
    quantity?: string;
}
