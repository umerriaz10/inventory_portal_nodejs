import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-inventory-rec-popup',
  templateUrl: './view-inventory-rec-popup.component.html',
  styleUrls: ['./view-inventory-rec-popup.component.scss']
})
export class ViewInventoryRecPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ViewInventoryRecPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
  }

  ok() {
    this.dialogRef.close();
  }

}
