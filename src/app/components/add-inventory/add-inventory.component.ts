import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IOption } from 'ng-select';
import * as _ from 'lodash';
import { ItemsService } from 'src/app/services/items/items.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as moment from 'moment';
import { UserService } from 'src/app/services/user/user.service';
import { InventoryRecord } from 'src/app/interfaces/inventory-record';
import { InventoryRecordService } from 'src/app/services/inventory-record/inventory-record.service';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.scss']
})
export class AddInventoryComponent implements OnInit {

  /**
   * This variable is used to store the items coming from parent component
   *
   * @memberof AddInventoryComponent
   */
  @Input() items;

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('myDateInput', {static: false}) myDateInput: jqxDateTimeInputComponent;

  /**
   * This variable is used to store the value of currently selected product
   *
   * @memberof AddInventoryComponent
   */
  selectedProductID = '';

  /**
   * This variable is used to store selected product quantity
   *
   * @memberof AddInventoryComponent
   */
  selectedProductQuantity = '';

  /**
   * This variable checks when the add button should be make disable
   *
   * @memberof AddInventoryComponent
   */
  addBtnDisable = true;

  /**
   * This array holds the options for products dropdown.
   *
   * @type {Array < IOption >}
   * @memberof AddInventoryComponent
   */
  products: Array<IOption> = [];

  /**
   * This array is used to store the selected products values
   *
   * @type {Array<any>}
   * @memberof AddInventoryComponent
   */
  selectedProducts: Array<any> = [];

  /**
   * This variable is used to store the items dropdown
   *
   * @memberof AddInventoryComponent
   */
  itemsDropDown: any = [];

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof AddInventoryComponent
   */
  simpleForm: FormGroup;

  /**
   * Validae the form
   *
   * @memberof AddInventoryComponent
   */
  submitted = false;

  /**
   * Holds the current value of an item
   *
   * @memberof AddInventoryComponent
   */
  itemCurrentValue: number;

  /**
   * Holds the new value of an item
   *
   * @memberof AddInventoryComponent
   */
  itemNewValue: number;

  /**
   * Flag to whether disable or enable the quantity field
   *
   * @memberof AddInventoryComponent
   */
  isDisable = true;

  /**
   * holds the error message to be displayed
   *
   * @type {string}
   * @memberof AddInventoryComponent
   */
  errMsg = '';

  /**
   * holds the record of each entry made by the user
   *
   * @type {InventoryRecord}
   * @memberof AddInventoryComponent
   */
  record: InventoryRecord;

  /**
   * holds the record of each entry made by the user
   *
   * @type {InventoryRecord}
   * @memberof AddInventoryComponent
   */
  recordItems: Array<any> = [];

  /**
   * This variable is used to set the max date in the datepicker
   *
   * @memberof TimeLogPopUpComponent
   */
  maxDate = new Date();

  /**
   * Holds the date that user selects
   *
   * @memberof AddInventoryComponent
   */
  selectedDate = new Date();

  isAdded = false;

  /**
   * Creates an instance of AddInventoryComponent.
   *
   * @param {FormBuilder} formBuilder
   * @param {ToastrService} toastr
   * @param {ItemsService} itemService
   * @memberof AddInventoryComponent
   */
  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    private itemService: ItemsService,
    private userService: UserService,
    private inventoryRecordService: InventoryRecordService
  ) { }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof AddInventoryComponent
   */
  ngOnInit() {
    this.setProducts();
    this.createForm();
  }

  /**
   * This method creates the form with all the relevant controls and validations.
   *
   * @memberof AddInventoryComponent
   */
  createForm() {
    this.simpleForm = this.formBuilder.group({
      product: [null, [Validators.required]],
      quantity: ['', [Validators.required]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  /**
   * Sets the dropdown values for products
   *
   * @memberof AddInventoryComponent
   */
  setProducts() {
    const products = [];
    this.items.forEach(item => {
      products.push({
        'label': item.p_name,
        'value': item.id
      });
    });
    this.products = products;
  }

  /**
   * This method is used to set ID of the product that is selected by the user
   *
   * @param event
   * @memberof AddInventoryComponent
   */
  setSelectedProductID(event) {
    if (event !== undefined) {
      this.selectedProductID = event.value;
      this.isAdded = true;
      if (this.selectedProductQuantity !== null && this.selectedProductQuantity !== '') {
        this.addBtnDisable = false;
      }
    } else {
      this.isAdded = false;
      this.simpleForm.get('product').reset();
      this.selectedProductQuantity = '';
      this.simpleForm.get('quantity').reset();
      this.addBtnDisable = true;
    }
  }

  /**
   * This method is used to set quantity of the selected product
   *
   * @param event
   * @memberof AddInventoryComponent
   */
  setSelectedProductQuantity(event) {
    this.submitted = false;
    // tslint:disable-next-line: radix
    const quantity = event.target.value;
    if (quantity !== null && quantity > 0) {
      this.selectedProductQuantity = quantity;
      if (this.selectedProductID !== '') {
        this.addBtnDisable = false;
      }
    } else if (quantity < 0) {
      this.selectedProductQuantity = null;
      this.setOptions('Quantity Should be a positive number', 'Enter a positive number');
    } else {
      this.selectedProductQuantity = null;
      this.submitted = true;
      this.addBtnDisable = true;
      this.errMsg = 'Quantity is Required';
    }
  }

  /**
   * This method is used to validate and add the product value, and quantity in the selectedProducts Array,
   * and reset DOM elements
   *
   * @returns
   * @memberof AddInventoryComponent
   */
  selectProduct() {
    if (this.selectedProductID === '' || this.selectedProductQuantity === '') {
      this.toastr.error('Please enter valid input for social media');
      return;
    }
    this.selectedProducts.push({
      'id': this.selectedProductID,
      'name': _.filter(this.products, obj => obj.value === this.selectedProductID)[0].label,
      // tslint:disable-next-line: radix
      'quantity': parseInt(this.selectedProductQuantity)
    });

    _.remove(this.products, obj => obj.value === this.selectedProductID);
    this.products = [...this.products];

    this.selectedProductID = this.selectedProductQuantity = '';
    this.addBtnDisable = true;
    this.isAdded = false;
    this.simpleForm.get('product').reset();
    this.simpleForm.get('quantity').reset();
  }

  /**
   * This method is used to delete a product row that is added by the user
   *
   * @param option
   * @memberof AddInventoryComponent
   */
  deleteEntry(option) {
    const dropdownProduct = {'label': option.name, 'value': option.id};
    this.products.push(dropdownProduct);
    this.products = [...this.products];
    _.remove(this.selectedProducts, obj => obj.id === option.id);
  }

  /**
   * This method is triggered when user clicks on any of the dropdowns in 'country', 'state', and 'city'
   * in order to keep the dropdown from opening upwards
   *
   * @param value
   * @memberof RegisterComponent
   */
  scrollOnDropdownExpansion(value) {
    window.scrollTo(0, value);
  }


  /**
   * Method called on the click of Add Inventory
   *
   * @memberof AddInventoryComponent
   */
  addItem() {
    // if (this.simpleForm.invalid) {
    //   this.toastr.error('Please fill in the required fields!');
    // }
    if (this.selectedProducts.length <= 0) {
      this.toastr.error('Please ADD any product to inventory!');
    } else {
      this.ngxService.start();
      this.setRecordData();
      this.inventoryRecordService.postInventoryRecord(this.record).subscribe(res => {
        const recInvId = res.data.insertId;
        this.selectedProducts.forEach(item => {
          const recordItems = [recInvId, item.id, item.name, item.quantity];
          this.recordItems.push(recordItems);
        });
        this.inventoryRecordService.postInventoryRecordItems(this.recordItems).subscribe(response => {
          this.updateItem();
        }, err => {
          this.toastr.error('Please repeat your action item due to this error:' + err);
          this.ngxService.stop();
        });
    }, err => {
      this.toastr.error('Please repeat your action item due to this error:' + err);
      this.ngxService.stop();
    });
    }
  }

  /**
   * This method is used to update the products inventory
   */
  updateItem() {
    const status = 'Added';
    this.selectedProducts.forEach(item => {
      this.itemService.updateItemQuantity(item.id, item.quantity, status).subscribe(res => {
      }, err => {
        this.toastr.error('Please repeat your action item due to this error:' + err);
        this.ngxService.stop();
      });
    });
    this.itemService.saveItemsLocalStorage(null);
    this.ngxService.stop();
    this.recordItems = [];
    this.selectedProducts = [];
  }

  /**
   * sets the options for input field of quantity
   *
   * @param {*} notifyMsg
   * @param {*} errMsg
   * @memberof AddInventoryComponent
   */
  setOptions(notifyMsg, errMsg) {
    this.simpleForm.get('quantity').reset();
    this.submitted = true;
    this.errMsg = errMsg;
    this.toastr.error(notifyMsg);
  }

  /**
   *  Set the record data and call record's service method
   *
   * @memberof AddInventoryComponent
   */
  setRecordData() {
    this.record = {
      'no_of_products' : this.selectedProducts.length,
      'status'   : 'Added',
      'date' : this.selectedDate,
      'addedBy' : this.userService.getUser().name
    };
    this.inventoryRecordService.postInventoryRecord(this.record);
  }

}
