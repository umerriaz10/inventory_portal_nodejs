import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { Vendors } from 'src/app/interfaces/vendors';
import { VendorsService } from 'src/app/services/vendors/vendors.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-add-vendor-popup',
  templateUrl: './add-vendor-popup.component.html',
  styleUrls: ['./add-vendor-popup.component.scss']
})
export class AddVendorPopupComponent implements OnInit {

/**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof TimeLogDetailComponent
   */
  addVendorForm: FormGroup;

  isValid = false;

  submitted = false;

  vendor: Vendors;

  constructor(
    public dialogRef: MatDialogRef<AddVendorPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private vendorService: VendorsService,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  /**
   * This method creates the form with all the relevant controls
   *
   * @memberof TimeLogDetailComponent
   */
  createForm() {
    this.addVendorForm = this.formBuilder.group({
      v_name: ['', Validators.required],
      v_email: [''],
      v_address: [''],
      v_contact_number: [''],
      v_balance: ['', [Validators.required, Validators.min(0)]],
      v_totalPaid: ['', [Validators.required, Validators.min(0)]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.addVendorForm.controls; }

  closeModal() {
    this.dialogRef.close();
  }


  addVendorWithFormCheck() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.addVendorForm.invalid) {
      this.toastr.error('Please Correctly fill up the form!');
      this.ngxService.stop();
      this.isValid = false;
    }
    if (this.isValid === true) {
      this.addVendor();
    }
  }

  addVendor() {
    const vendor = this.setValues(this.addVendorForm.controls);
    this.vendorService.postVendor(vendor).subscribe(res => {
      this.addVendorForm.reset();
      this.toastr.success('Vendor is added successfully!!');
      window.localStorage.setItem('vendors', null);
      this.ngxService.stop();
      this.data.function();
      this.dialogRef.close();
    }, err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  setValues(form) {
    this.vendor = {
      'v_name': form.v_name.value,
      'v_email': form.v_email.value,
      'v_address': form.v_address.value,
      'v_contact_number': form.v_contact_number.value,
      'v_balance': form.v_balance.value,
      'v_totalPaid': form.v_totalPaid.value,
      'v_paidToVendor': 0,
      'v_addedBy': this.userService.getUser().name
    };
    return this.vendor;
  }

}
