import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVendorPopupComponent } from './add-vendor-popup.component';

describe('AddVendorPopupComponent', () => {
  let component: AddVendorPopupComponent;
  let fixture: ComponentFixture<AddVendorPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVendorPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVendorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
