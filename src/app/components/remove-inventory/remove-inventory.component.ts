import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IOption } from 'ng-select';
import * as _ from 'lodash';
import { ItemsService } from 'src/app/services/items/items.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user/user.service';
import * as moment from 'moment';
import { InventoryRecord } from 'src/app/interfaces/inventory-record';
import { InventoryRecordService } from 'src/app/services/inventory-record/inventory-record.service';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { Items } from 'src/app/interfaces/items';



@Component({
  selector: 'app-remove-inventory',
  templateUrl: './remove-inventory.component.html',
  styleUrls: ['./remove-inventory.component.scss']
})
export class RemoveInventoryComponent implements OnInit {

  /**
   * This variable is used to store the items coming from parent component
   *
   * @memberof RemoveInventoryComponent
   */
  @Input() items;

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('myDateInput', {static: false}) myDateInput: RemoveInventoryComponent;

  /**
   * This variable is used to store the value of currently selected product
   *
   * @memberof RemoveInventoryComponent
   */
  selectedProductID = '';

  /**
   * This variable is used to store selected product quantity
   *
   * @memberof RemoveInventoryComponent
   */
  selectedProductQuantity = '';

  /**
   * This variable checks when the add button should be make disable
   *
   * @memberof RemoveInventoryComponent
   */
  addBtnDisable = true;

  /**
   * This array holds the options for products dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RemoveInventoryComponent
   */
  dropdownProducts: Array<IOption> = [];

  /**
   * This array holds the options for products dropdown.
   *
   * @type {Array < IOption >}
   * @memberof RemoveInventoryComponent
   */
  products: Array<Items> = [];

  /**
   * This array is used to store the selected products values
   *
   * @type {Array<any>}
   * @memberof RemoveInventoryComponent
   */
  selectedProducts: Array<any> = [];

  /**
   * This variable is used to store the items dropdown
   *
   * @memberof RemoveInventoryComponent
   */
  itemsDropDown: any = [];

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof RemoveInventoryComponent
   */
  simpleForm: FormGroup;

  /**
   * Validae the form
   *
   * @memberof RemoveInventoryComponent
   */
  submitted = false;

  /**
   * Holds the current value of an item
   *
   * @memberof RemoveInventoryComponent
   */
  itemCurrentValue: number;

  /**
   * Holds the new value of an item
   *
   * @memberof RemoveInventoryComponent
   */
  itemNewValue: number;

  /**
   * Flag to whether disable or enable the quantity field
   *
   * @memberof RemoveInventoryComponent
   */
  isDisable = true;

  /**
   * holds the error message to be displayed
   *
   * @type {string}
   * @memberof RemoveInventoryComponent
   */
  errMsg = '';

  /**
   * holds the record of each entry made by the user
   *
   * @type {InventoryRecord}
   * @memberof RemoveInventoryComponent
   */
  record: InventoryRecord;

  /**
   * holds the record of each entry made by the user
   *
   * @type {InventoryRecord}
   * @memberof RemoveInventoryComponent
   */
  recordItems: Array<any> = [];

  /**
   * Holds the date that user selects
   *
   * @memberof RemoveInventoryComponent
   */
  selectedDate = new Date();

  isAdded = false;

  selectedCurrQuantity;

  /**
   * Creates an instance of AddInventoryComponent.
   *
   * @param {FormBuilder} formBuilder
   * @param {ToastrService} toastr
   * @param {ItemsService} itemService
   * @memberof AddInventoryComponent
   */
  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    private userService: UserService,
    private itemService: ItemsService,
    private inventoryRecordService: InventoryRecordService
  ) { }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof RemoveInventoryComponent
   */
  ngOnInit() {
    this.setProducts();
    this.createForm();
  }

  /**
   * This method creates the form with all the relevant controls and validations.
   *
   * @memberof RemoveInventoryComponent
   */
  createForm() {
    this.simpleForm = this.formBuilder.group({
      product: [null, [Validators.required]],
      quantity: ['', [Validators.required]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof RemoveInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  /**
   * Sets the dropdown values for products
   *
   * @memberof RemoveInventoryComponent
   */
  setProducts() {
    const products = [];
    this.items.forEach(item => {
      products.push({
        'label': item.p_name,
        'value': item.id
      });
    });
    this.dropdownProducts = products;
  }

  /**
   * This method is used to set ID of the product that is selected by the user
   *
   * @param event
   * @memberof RemoveInventoryComponent
   */
  setSelectedProductID(event) {
    if (event !== undefined) {
      this.isAdded = true;
      this.selectedProductID = event.value;
      this.selectedCurrQuantity =  _.filter(this.items, obj => obj.id === this.selectedProductID)[0].p_quantity;
      if (this.selectedProductQuantity !== null && this.selectedProductQuantity !== '') {
        this.addBtnDisable = false;
      }
    } else {
      this.selectedProductQuantity = '';
      this.simpleForm.get('quantity').reset();
      this.addBtnDisable = true;
    }
  }

  /**
   * This method is used to set quantity of the selected product
   *
   * @param event
   * @memberof RemoveInventoryComponent
   */
  setSelectedProductQuantity(event) {
    this.submitted = false;
    // tslint:disable-next-line: radix
    const quantity = event.target.value;
    if (quantity !== null && quantity < this.selectedCurrQuantity && quantity > 0) {
      this.selectedProductQuantity = quantity;
      if (this.selectedProductID !== '') {
        this.addBtnDisable = false;
      }
    } else if (quantity < 0) {
      this.selectedProductQuantity = null;
      this.setOptions('Quantity Should be a positive number', 'Enter a positive number');
    } else if (quantity > this.selectedCurrQuantity) {
      this.setOptions('Quantity Should be less than the Available quantity', 'Enter a valid number');
    } else {
      this.selectedProductQuantity = null;
      this.submitted = true;
      this.errMsg = 'Quantity is Required';
    }
  }

  /**
   * This method is used to validate and add the product value, and quantity in the selectedProducts Array,
   * and reset DOM elements
   *
   * @returns
   * @memberof RemoveInventoryComponent
   */
  selectProduct() {
    if (this.selectedProductID === '' || this.selectedProductQuantity === '') {
      this.toastr.error('Please enter valid input for social media');
      return;
    }
    this.selectedProducts.push({
      'id': this.selectedProductID,
      'name': _.filter(this.dropdownProducts, obj => obj.value === this.selectedProductID)[0].label,
      // tslint:disable-next-line: radix
      'quantity': parseInt(this.selectedProductQuantity)
    });

    _.remove(this.dropdownProducts, obj => obj.value === this.selectedProductID);
    this.dropdownProducts = [...this.dropdownProducts];

    this.selectedProductID = this.selectedProductQuantity = '';
    this.addBtnDisable = true;
    this.isAdded = false;
    this.simpleForm.get('product').reset();
    this.simpleForm.get('quantity').reset();
  }

  /**
   * This method is used to delete a product row that is added by the user
   *
   * @param option
   * @memberof RemoveInventoryComponent
   */
  deleteEntry(option) {
    const dropdownProduct = {'label': option.name, 'value': option.id};
    this.dropdownProducts.push(dropdownProduct);
    this.dropdownProducts = [...this.dropdownProducts];
    _.remove(this.selectedProducts, obj => obj.id === option.id);
  }

  /**
   * This method is triggered when user clicks on any of the dropdowns in 'country', 'state', and 'city'
   * in order to keep the dropdown from opening upwards
   *
   * @param value
   * @memberof RemoveInventoryComponent
   */
  scrollOnDropdownExpansion(value) {
    window.scrollTo(0, value);
  }

  /**
   * Method called on the click of remove button in order to remove the
   * mentioned quantity for that item
   *
   * @memberof RemoveInventoryComponent
   */
  removeItem() {
    this.submitted = true;
    if (this.selectedProducts.length <= 0) {
      this.toastr.error('Please REMOVE any product to inventory!');
    } else {
      this.ngxService.start();
      this.setRecordData();
      this.inventoryRecordService.postInventoryRecord(this.record).subscribe(res => {
        const recInvId = res.data.insertId;
        this.selectedProducts.forEach(item => {
          const recordItems = [recInvId, item.id, item.name, item.quantity];
          this.recordItems.push(recordItems);
      });
      this.inventoryRecordService.postInventoryRecordItems(this.recordItems).subscribe(response => {
          this.updateItem();
        }, err => {
          this.toastr.error('Please repeat your action item due to this error:' + err);
          this.ngxService.stop();
        });
    }, err => {
      this.toastr.error('Please repeat your action item due to this error:' + err);
      this.ngxService.stop();
    });
    }
  }
   /**
   * This method is used to update the products inventory
   */
  updateItem() {
    const status = 'Removed';
    this.selectedProducts.forEach(item => {
      this.itemService.updateItemQuantity(item.id, item.quantity, status).subscribe(res => {
      this.itemService.saveItemsLocalStorage(null);
      this.ngxService.stop();
      }, err => {
        this.toastr.error('Please repeat your action item due to this error:' + err);
        this.ngxService.stop();
      });
    });
    this.recordItems = [];
    this.selectedProducts = [];
  }

  /**
   * Method called on the selection of the item from the drop down
   *
   * @param {*} event
   * @memberof RemoveInventoryComponent
   */
  itemIsSelected(event) {
    this.submitted = false;
    if (event !== undefined) {
      this.simpleForm.get('quantity').enable();
      this.itemNewValue = this.itemCurrentValue = event.id;
    } else {
      this.itemNewValue = null;
      this.simpleForm.get('quantity').disable();
    }
    this.simpleForm.get('quantity').reset();
  }

  /**
   * Method to update the Current item quantity on keyup
   *
   * @param {*} value
   * @memberof RemoveInventoryComponent
   */
  currValue(value) {
    this.submitted = false;
    // tslint:disable-next-line: radix
    const quantity = parseInt(value);
    if (quantity !== null && quantity < this.itemCurrentValue && quantity > 0) {
      this.itemNewValue = this.itemCurrentValue - quantity;
    } else if (quantity < 0) {
      this.setOptions('Quantity Should be a positive number', 'Enter a positive number');
    } else if (quantity > this.itemCurrentValue) {
      this.setOptions('Quantity Should be less than the Available quantity', 'Enter a valid number');
    } else {
      this.itemNewValue = this.itemCurrentValue;
      this.submitted = true;
      this.errMsg = 'Quantity is Required';
    }
  }

  /**
   * sets the options for input field of quantity
   *
   * @param {*} notifyMsg
   * @param {*} errMsg
   * @memberof RemoveInventoryComponent
   */
  setOptions(notifyMsg, errMsg) {
    this.simpleForm.get('quantity').reset();
    this.submitted = true;
    this.addBtnDisable = true;
    this.errMsg = errMsg;
    this.toastr.error(notifyMsg);
  }

  // /**
  //  * updates the Dropdown with updated value by fetching updated data from firestore
  //  *
  //  * @memberof RemoveInventoryComponent
  //  */
  // updateItemsDropDown() {
  //   this.itemService.getItems().subscribe(res => {
  //     this.items = res;
  //     this.setItemsDropDown();
  //   });
  // }

  /**
   *  Set the record data and call record's service method
   *
   * @memberof AddInventoryComponent
   */
  setRecordData() {
    this.record = {
      'no_of_products' : this.selectedProducts.length,
      'status'   : 'Removed',
      'date' : this.selectedDate,
      'addedBy' : this.userService.getUser().name
    };
    this.inventoryRecordService.postInventoryRecord(this.record);
  }

}
