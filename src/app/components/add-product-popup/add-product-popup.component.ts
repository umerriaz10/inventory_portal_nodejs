import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user/user.service';
import { Items } from 'src/app/interfaces/items';
import { ItemsService } from 'src/app/services/items/items.service';

@Component({
  selector: 'app-add-product-popup',
  templateUrl: './add-product-popup.component.html',
  styleUrls: ['./add-product-popup.component.scss']
})
export class AddProductPopupComponent implements OnInit {

/**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof TimeLogDetailComponent
   */
  addProductForm: FormGroup;

  isValid = true;

  submitted = false;

  product: Items;


  constructor(
    public dialogRef: MatDialogRef<AddProductPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private itemService: ItemsService,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  /**
   * This method creates the form with all the relevant controls
   *
   * @memberof TimeLogDetailComponent
   */
  createForm() {
    this.addProductForm = this.formBuilder.group({
      p_name: ['', Validators.required],
      p_quantity: ['', [Validators.required, Validators.min(0)]],
      p_price: ['', [Validators.required, Validators.min(0)]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.addProductForm.controls; }

  closeModal() {
    this.dialogRef.close();
  }


  addProductWithFormCheck() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.addProductForm.invalid) {
      this.toastr.error('Please Check Your Form Fields!');
      this.ngxService.stop();
      this.isValid = false;
    }
    if (this.isValid === true) {
      this.addProduct();
    }
  }

  addProduct() {
    const product = this.setValues(this.addProductForm.controls);
    this.itemService.postItem(product).subscribe(res => {
      this.addProductForm.reset();
      this.toastr.success('Product is added successfully!!');
      this.itemService.saveItemsLocalStorage(null);
      this.ngxService.stop();
      this.data.function();
      this.dialogRef.close();
    }, err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  setValues(form) {
    this.product = {
      'p_name': form.p_name.value,
      'p_quantity': form.p_quantity.value,
      'p_price': form.p_price.value,
      'p_addedBy': this.userService.getUser().name
    };
    return this.product;
  }

}
