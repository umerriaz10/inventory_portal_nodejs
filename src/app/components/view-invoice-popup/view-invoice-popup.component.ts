import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-invoice-popup',
  templateUrl: './view-invoice-popup.component.html',
  styleUrls: ['./view-invoice-popup.component.scss']
})
export class ViewInvoicePopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ViewInvoicePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
  }

}
