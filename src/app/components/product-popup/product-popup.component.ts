import { Component, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
@Component({
  selector: 'app-product-popup',
  templateUrl: './product-popup.component.html',
  styleUrls: ['./product-popup.component.scss']
})
export class ProductPopupComponent {

  product: any = {
    'item_id': 0,
    'item_name': '',
    'quantity': 0,
    'item_price': 0,
    'total_amount': 0
  };

   /**
   * This variable is used to set configuration of the modal.
   *
   * @memberof ConfirmationPopUpComponent
   */
  config = {
    backdrop: false,
    ignoreBackdropClick: true,
    keyboard: false
  };

  selectedProductQuantity = 0;

  itemAvaiableQuantity = 0;

  selectedProductID = 0;

  isAdded = false;

  addBtnDisable = true;

  isQuanErr = false;

  quanErrMsg = '';

  priceErrMsg = '';

  isPriceErr = false;

  constructor(
    public dialogRef: MatDialogRef<ProductPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private toastr: ToastrService
  ) { }

  addProduct() {
    if (this.product.quantity === 0 || this.product.item_id === 0) {
      this.toastr.error('Please Enter Valid Values');
    } else {
      this.product.total_amount = this.product.item_price * this.product.quantity;
      debugger
      this.dialogRef.close(this.product);
    }
  }

  closeModal() {
    this.dialogRef.close();
  }

  /**
   * This method is used to set ID of the product that is selected by the user
   *
   * @param event
   * @memberof RegisterComponent
   */
  setSelectedProductID(event) {
    if (event !== undefined) {
      this.isAdded = true;
      const ddProduct = _.filter(this.data.products, obj => obj.id === event.value)[0];
      this.product.item_price = ddProduct.p_price;
      this.itemAvaiableQuantity = ddProduct.p_quantity;
      this.product.item_id = event.value;
      this.product.item_name = event.label;
    } else {
      this.isAdded = false;
      this.addBtnDisable = true;
      this.product.quantity = this.product.item_price = 0;
    }
  }

  /**
   * This method is used to set the link entered by the user (triggered on keyup in input)
   *
   * @param event
   * @memberof RegisterComponent
   */
  setSelectedProductQuantity(event) {
    this.addBtnDisable = true;
    this.isQuanErr = true;
    const quantity = event.target.value;
    if (quantity !== 0 && quantity > 0) {
      this.product.quantity = quantity;
      this.isQuanErr = false;
    } else if (quantity < 0) {
      this.quanErrMsg = 'Enter a positive Quantity';
      this.toastr.warning('Quantity Should be a positive number');
    } else if (quantity > this.itemAvaiableQuantity) {
      this.toastr.warning('Please enter Quantity less than the Available Quantity');
      this.quanErrMsg = 'Enter a valid Quantity';
    } else {
      this.quanErrMsg = 'Quantity is Required';
    }
  }

  /**
   * This method is used to set the link entered by the user (triggered on keyup in input)
   *
   * @param event
   * @memberof RegisterComponent
   */
  setSelectedProductPrice(event) {
    this.addBtnDisable = true;
    this.isPriceErr = true;
    const price = event.target.value;
    if (price !== 0 && price > 0) {
      this.product.item_price = price;
      this.isQuanErr = false;
      this.addBtnDisable = false;
      this.priceErrMsg = '';
    } else if (price < 0) {
      this.priceErrMsg = 'Enter a positive Price';
      this.toastr.warning('Price Should be a positive number');
    } else {
      this.priceErrMsg = 'Enter a valid Price';
    }
  }

  /**
   * This method is triggered when user clicks on any of the dropdowns in 'country', 'state', and 'city'
   * in order to keep the dropdown from opening upwards
   *
   * @param value
   * @memberof RegisterComponent
   */
  scrollOnDropdownExpansion(value) {
    window.scrollTo(0, value);
  }

}
