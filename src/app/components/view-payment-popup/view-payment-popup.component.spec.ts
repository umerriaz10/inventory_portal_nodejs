import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPaymentPopupComponent } from './view-payment-popup.component';

describe('ViewPaymentPopupComponent', () => {
  let component: ViewPaymentPopupComponent;
  let fixture: ComponentFixture<ViewPaymentPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPaymentPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPaymentPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
