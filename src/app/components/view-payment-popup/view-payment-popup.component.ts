import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-payment-popup',
  templateUrl: './view-payment-popup.component.html',
  styleUrls: ['./view-payment-popup.component.scss']
})
export class ViewPaymentPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ViewPaymentPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
  }

  ok() {
    this.dialogRef.close();
  }

}
