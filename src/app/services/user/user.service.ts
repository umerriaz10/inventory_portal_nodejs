import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtService } from '../jwt/jwt.service';
import { AdapterService } from '../adapter/adapter.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(
    private router: Router,
    private adapter: AdapterService,
    private jwtService: JwtService
  ) { }

  /**
   * Get the details of the user
   *
   * @param {*} id
   * @memberof UserService
   */
  login(cred) {
    return this.adapter.post('/admin/login', cred);
  }

  /**
   * Save the user after login in local storage
   *
   * @param {*} user
   * @memberof UserService
   */
  saveUser(user) {
    window.localStorage.setItem('user', JSON.stringify(user));
  }

  /**
   * Get the user after login from local storage
   *
   * @returns
   * @memberof UserService
   */
  getUser() {
    const user = JSON.parse(window.localStorage['user']);
    if (user !== undefined && user !== null) {
      return user;
    } else {
      return null;
    }
  }
}
