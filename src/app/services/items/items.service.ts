import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Items } from 'src/app/interfaces/items';
import { Observable } from 'rxjs';
import { AdapterService } from '../adapter/adapter.service';

/**
 * Service that have the required functions for the items module.
 *
 * @export
 * @class ItemsService
 */
@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  /**
   * Creates an instance of ItemsService.
   *
   * @param {ToastrService} toastr
   * @memberof ItemsService
   */
  constructor(
    private adapter: AdapterService,
    private toastr: ToastrService,
  ) { }

  /**
   * Save the products in local storage
   *
   * @memberof ItemsService
   */
  saveItemsLocalStorage(items) {
    window.localStorage.setItem('items', JSON.stringify(items));
  }

  /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getItemsFromLocal() {
    const items = window.localStorage.getItem('items');
    if (items !== 'undefined' && items !== null) {
      return JSON.parse(items);
    } else {
      return null;
    }
  }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getItems(): Observable<any> {
    return this.adapter.get('/products', null, null);
  }

  /**
   * Get a specific product by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getProductById(id): Observable<any> {
    return this.adapter.get('/products/' + id, null, null);
  }

  /**
   * Post the item in the DB
   *
   * @memberof ItemsService
   */
  postItem(item): Observable<any> {
    return this.adapter.post('/products', item, null);
  }

  /**
   * Method to update the item from the DB
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateItemQuantity(id: string, value, status) {
    const data = {'p_quantity' : value, 'id' : id, 'status' : status};
    return this.adapter.patch('/products/updateQuantity', data, null);
  }

  /**
   * Method to update the item from the DB
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateItem(product) {
    return this.adapter.patch('/products/updateQuantity', product, null);
  }

  removeProduct(product): Observable<any> {
    return this.adapter.delete('/products/' + product.id, null);
  }

}
