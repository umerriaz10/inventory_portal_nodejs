import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JwtService } from 'src/app/services/jwt/jwt.service';

/**
 * This injector provides the auth-guard service throughout the application.
 */
@Injectable({
  providedIn: 'root'
})

/**
 * The auth guard service is used to prevent unauthenticated users from
 * accessing restricted routes, it's used in app.routing.ts to protect the home
 * page route
 */
export class AuthGuardService implements CanActivate {
  /**
   * The constructor initializes the ToastrService & TranslatePipe in the component.
   */
  constructor(private toastr: ToastrService,
              private jwt: JwtService,
              private router: Router) {}

  /**
   * This method checks if the user is authorized to access a certain route by checking the auth
   * token in the cache.
   *
   * @return
   */
  canActivate() {
    if (this.jwt.getToken() !== null) {
      return true;
    }
    this.router.navigate(['']);
    return false;
  }
}
