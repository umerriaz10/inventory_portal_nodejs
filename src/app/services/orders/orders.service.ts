import { Injectable } from '@angular/core';
import { AdapterService } from '../adapter/adapter.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  /**
   * Creates an instance of ItemsService.
   *
   * @param {ToastrService} toastr
   * @memberof ItemsService
   */
  constructor(
    private adapter: AdapterService
  ) { }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  postOrder(order): Observable<any> {
    return this.adapter.post('/order', order, null);
  }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  postOrderItem(orderItem): Observable<any> {
    return this.adapter.post('/orderItem', orderItem, null);
  }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getOrders(): Observable<any> {
    return this.adapter.get('/order', null, null);
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getOrderById(id): Observable<any> {
    return this.adapter.get('/order/' + id, null, null);
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getOrdersByVendorId(id): Observable<any> {
    return this.adapter.get('/order/' + id + '/' + id, null, null);
  }

   /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  saveOrdersLocalStorage(orders) {
    window.localStorage.setItem('orders', JSON.stringify(orders));
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getOrderItemById(id): Observable<any> {
    return this.adapter.get('/orderItem/' + id, null, null);
  }

  removeOrderItem(orderItem): Observable<any> {
    return this.adapter.delete('/orderItem/' + orderItem.order_id + '/' + orderItem.item_id , null);
  }

  removeOrder(order): Observable<any> {
    return this.adapter.delete('/order/' + order.id, null);
  }

  /**
   * Method to update the item from the firestore
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateOrder(order) {
    return this.adapter.patch('/order', order, null);
  }

   /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getOrdersFromLocal() {
    const orders = JSON.parse(window.localStorage.getItem('orders'));
    if (orders !== undefined && orders !== null) {
      return orders;
    } else {
      return null;
    }
  }
}
