import { Injectable } from '@angular/core';
import { AdapterService } from '../adapter/adapter.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  constructor(
    private adapter: AdapterService
  ) { }

  /**
   * post a new vendor in the DB
   *
   * @memberof VendorsService
   */
  postPayment(payment): Observable<any> {
    return this.adapter.post('/payments', payment, null);
  }

   /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getPayments(): Observable<any> {
    return this.adapter.get('/payments', null, null);
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getPaymentsById(id): Observable<any> {
    return this.adapter.get('/payments/' + id, null, null);
  }


  removePayment(payment): Observable<any> {
    return this.adapter.delete('/payments/' + payment.id, null);
  }

  /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  savePaymentsLocalStorage(payments) {
    window.localStorage.setItem('payments', JSON.stringify(payments));
  }

  /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getPaymentsFromLocal() {
    const payments = JSON.parse(window.localStorage.getItem('payments'));
    if (payments !== undefined && payments !== null) {
      return payments;
    } else {
      return null;
    }
  }
}
