import { Injectable } from '@angular/core';
import { Vendors } from 'src/app/interfaces/vendors';
import { AdapterService } from '../adapter/adapter.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VendorsService {

  /**
   * Holds the items coming from firestore
   *
   * @type {Array<Vendors>}
   * @memberof VendorsService
   */
  vendors: Array<Vendors> = [];

  /**
   * Creates an instance of VendorsService.
   *
   * @param {ToastrService} toastr
   * @memberof VendorsService
   */
  constructor(
    private adapter: AdapterService
  ) { }

  /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  saveVendorsLocalStorage(vendors) {
    window.localStorage.setItem('vendors', JSON.stringify(vendors));
  }

  /**
   * Get the details of the vendors
   *
   * @memberof VendorsService
   */
  getVendors(): Observable<any> {
    return this.adapter.get('/vendors', null, null);
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getVendorById(id): Observable<any> {
    return this.adapter.get('/vendors/' + id, null, null);
  }

  /**
   * post a new vendor in the DB
   *
   * @memberof VendorsService
   */
  postVendor(vendor): Observable<any> {
    return this.adapter.post('/vendors', vendor, null);
  }

  updateVendorBalance(data): Observable<any> {
    return this.adapter.patch('/vendors/updateBalance', data, null);
  }

  /**
   * Method to update the item from the firestore
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateVendor(vendor) {
    return this.adapter.patch('/vendors', vendor, null);
  }

  /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getVendorsFromLocal() {
    const vendors = JSON.parse(window.localStorage.getItem('vendors'));
    if (vendors !== undefined && vendors !== null) {
      return vendors;
    } else {
      return null;
    }
  }
}
