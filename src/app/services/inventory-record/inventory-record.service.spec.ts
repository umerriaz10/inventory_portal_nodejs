import { TestBed } from '@angular/core/testing';

import { InventoryRecordService } from './inventory-record.service';

describe('InventoryRecordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InventoryRecordService = TestBed.get(InventoryRecordService);
    expect(service).toBeTruthy();
  });
});
